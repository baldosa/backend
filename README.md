# Colmena Backend

<p align="center" style="margin-top: 14px;">
  <a href="https://gitlab.com/colmena-project/dev/backend/-/pipelines?page=1&scope=all&ref=dev">
    <img
      src="https://gitlab.com/colmena-project/dev/backend/badges/dev/pipeline.svg?key_text=dev%20pipeline&key_width=80"
      alt="Dev Pipeline Status"
    >
  </a>
  <a href="https://gitlab.com/colmena-project/dev/backend/-/pipelines?page=1&scope=all&ref=main">
    <img
      src="https://gitlab.com/colmena-project/dev/backend/badges/main/pipeline.svg?key_text=staging%20pipeline&key_width=96"
      alt="Staging Pipeline Status"
    >
  </a>
  <a href="https://gitlab.com/colmena-project/dev/backend/-/blob/dev/LICENSE">
    <img
      src="https://img.shields.io/badge/License-GPL%20v3-white.svg"
      alt="License"
    >
  </a>
</p>

`colmena-backend` is a django application that exposes a public REST API for
the colmena frontend to communicate with.

## Requirements

The backend needs the python language to be installed. You can install it manually or via the `asdf` package manager, using the fixed version from the *.tool-versions* files.

* Ubuntu/Debian SO
* Python: `3.9.0` or greater
* asdf: (optional) [installation instructions](https://asdf-vm.com/guide/getting-started.html#_2-download-asdf)
* Docker [`24.0.2`](https://docs.docker.com/desktop/install/ubuntu/) (optional): used to run dockerized images of the backend
* Compose plugin [`v2.3.3`](https://docs.docker.com/compose/install/linux/) (optional)

This project depends on a recent version of django, and a few extra packages.

### SO requirements

`libsasl2-dev`

Install via apt:

```bash
sudo apt-get install libsasl2-dev
```

`gettext`

Used to generate translations via `make translations`. Install via apt:

```bash
sudo apt-get install gettext
```

Used to run tests with watch:

```bash
sudo apt-get install inotify-tools
```

### Asdf dependencies

Install the following plugins to manage the various languages involved in development:

* asdf plugin-add python

Then run `asdf install` in the root dir to have them installed.

### Dependencies

The backend server depends on a postgres database (for the local development environment) and a nextcloud instance. Go back to the [colmena-devops repository](https://gitlab.com/colmena-project/dev/colmena-devops/-/blob/main/README.md) and set them up to continue.

## Getting started

### Local Environment

Make a copy of the base env file.

```bash
cp .env.example .env
```

There's currently no need of updating any variable, but here's the definition for each of them:

#### Administration configuration

* `DEBUG_TOOLS`: whether the debug toolbar is available or not.
* `USE_COVERAGE`: whether the coverage report is enabled or not.
* `SUPERADMIN_EMAIL`: the superadmin email. This left side of this value should match the choosen username for the Nextcloud admin. For example, if your nextcloud admin is called superadmin, then a valid email would be `superadmin@anything.org`.
* `SUPERADMIN_PASSWORD`: the superadmin password. This value should match the choosen password for the Nextcloud user.
* `ASYNC_CLIENT_TIMEOUT`: the assigned timeout for the async http client. Default: `60.0` (float).
* `FETCH_NEW_TALK_MESSAGES_TIMEOUT` : the assigned wait time for nextcloud to fetch new messages. Default: `3` (integer).
* `ACCESS_TOKEN_LIFETIME`: optional duration for the jwt token expiration expressed in minutes. Default: `1440` (integer).
* `REFRESH_TOKEN_LIFETIME`: optional duration for the jwt refresh token expiration expressed in minutes. Default: `2880` (integer).
* `DJANGO_LOG_LEVEL`: the desired log level. Default: `DEBUG`. Options: `DEBUG`, `INFO`, `WARNING`, `ERROR`, `CRITICAL`.
* `BACKEND_HOSTNAME`: the backend hostname used to configure sites fixtures. Example: `http://localhost:8000`. Required.
* `FRONTEND_HOSTNAME`: the frontend hostname used to configure sites fixtures. Example: `http://localhost:5173`. Required.

#### Postgres

* `POSTGRES_DATABASE`: The postgres database name for the local development environment. Default: `colmena_dev`
* `POSTGRES_HOSTNAME`: The postgres hostname. Default: `localhost`
* `POSTGRES_USERNAME`: The postgres username. Default: `postgres`
* `POSTGRES_PASSWORD`: The postgres password. Default: `postgres`
* `POSTGRES_PORT`: The postgres port. Default: `5432`

#### Virtual Env

* `PYTHON`: path to the python binary in a virtual env.

#### Environment

* `DJANGO_SETTINGS_MODULE`: The settings module for the local development environment.
* `COLMENA_SECRET_KEY`: Secret key for the django project.
* `PASSWORD_RESET_TIMEOUT`: The number of seconds a password reset link is valid for. Default: `86400` (24hs)
* `IMAGE_MAX_SIZE`: The maximum size of the image in megabytes that can be used. Default: `2` (2mb)

#### Nextcloud

* `NEXTCLOUD_ADMIN_USER`: The default admin user for the local development nextcloud instance.
* `NEXTCLOUD_ADMIN_PASSWORD`: The admin password.
* `NEXTCLOUD_API_URL`: the nextcloud api url to use the REST API from the nextcloud async lib
* `NEXTCLOUD_API_WRAPPER_URL`: the nextcloud api wrapper url to use the custom REST APIs from occ.
* `NEXTCLOUD_ADMIN_GROUP_NAME`: the nextcloud admin group name
* `NEXTCLOUD_DEFAULT_USER_QUOTA`: the quota size for users to upload files.

We use `make` to quickly run predefined tasks to setup, start and stop our environment.

```bash
# Type make to display the default help message.
# You'll notice a bunch of suggestions that you can use to manage the database, skip them for now, they'll be useful as
# a reminder.
make
```

Create the virtualenv. This is a one-time only command.

```bash
make venv
```

Install python requirements

```bash
make setup
```

Run unit tests

```bash
make test
```

Run the django server

```bash
make server
```

Or you can run the dockerized version

```bash
make devops.up
```

### Production Environment

Make a copy of the base production env file.

```bash
cp .env.prod.example .env.prod
```

When testing with the `make docker...` commands make sure you edit the hosts variables properly to match the service names provided by docker. We also use `0.0.0.0` in some cases.

## Server Configuration

### Sites configuration

The server ships with five sites by default. These sites define the domains that will be used for different purposes. For example, the `backend-domain` domains is used to build urls for emails. Upon server startup the sites fixtures are loaded using the values provided by `BACKEND_HOSTNAME` and `FRONTEND_HOSTNAME`. It's a system administrator responsibility to check the server has started with the desired site names, so that emails are built properly.

Here's the brief definition for every predefined site in the `settings.base` module:

* `BACKEND_SITE_NAME`: the name of the domain that is used to send email invitations to the system administration. For example: `https://server.organization.net`.
* `FRONTEND_INVITATION_SITE_NAME`: the name of the domain that is used to send email invitations to an external client application (the colmena frontend application). For example: `https://client.organization.net`.
* `FRONTEND_SIGNUP_SITE_NAME`: the url used to build frontend invitation links.
* `FRONTEND_PASSWORD_RESET_SITE_NAME`: then url used to build password reset links for the frontend.
* `BACKEND_PASSWORD_RESET_SITE_NAME`: the url used to build password reset links for the backend.

To setup new domain values for these or any other keys, visit the __Sites__ section in backend admin site with a user that has __Superadmin__ privileges and change the existing values to the ones that meet your domains requirements.

## Structure of this project

There is a single django project, called `colmena-project`. According to the needs of
the frontend, we develop modules as separated django `apps` that can be tested
independently.

This is the current list of django apps that can be used within the `colmena` backend, found under `apps/`:

* `account`: User and group modelling. Authentication against the enabled backends (LDAP).
* `nextcloud`: Communication with the nextcloud instance.
* `instance`: Contains public details about a particular colmena instance.
* `cms`(optional): The backend for publications. Interfaces with the strapi CMS.
* `api`: a `REST` api that is the only interface that the colmena frontend has
  to configure (see Glossary).

## Documentation

Live documentation for the API can be found at:

```bash
# Redoc
http://localhost:8000/api/schema/redoc
# Swagger
http://localhost:8000/api/schema/swagger-ui
```

You can browse the docs in the dev environment too: [swagger](https://backend.dev.colmena.network/api/schema/swagger-ui/), [redoc](https://backend.dev.colmena.network/api/schema/redoc/)

## Development

### e-mail

There's a dummy email server that runs with docker-compose as part of the `make integration-service`. The default `settings.py` that we use for development is configured to point to its local port. Read [mailcrab documentation](https://github.com/tweedegolf/mailcrab) if you need to access its REST api for tests.
