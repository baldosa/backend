# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
default:
  image: ubuntu:20.04
  cache:
    paths:
    - "~/.cache/pip/"
stages:
  - test
  - publish
  - deploy

# ------------------------------------------------------------------------------
# Test jobs
#

.python: &python
  image: python:3.10-alpine
  before_script:
    - apk add --no-cache alpine-sdk python3-dev gettext
    - pip install -U pip
    - pip install -r requirements/${ENV}.txt
    - python -m openapi_python_generator apps/nextcloud/openapi/schema.json apps/nextcloud/openapi/client
    - python ./manage.py makemessages -l en -l es -i venv -e py,html -d django
    - git diff --quiet --exit-code || STATUS=$?
    - if [ "$STATUS" -ne 0 ]; then
        echo "❌ Looks like you forgot to commit something 😮";
        git diff --name-only;
        exit 1;
      fi

dev:
  extends: .python
  stage: test
  variables:
    ENV: dev
  script:
    - black --check .
  only:
    - dev
    - main
    # Match any branch name that follows our branch naming convention
    - /^\d{1,4}-.*$/
    - /^feature/.*$/
    - /^hotfix/.*$/
    - /^\d+\.\d+\.\d+$/

test:
  extends: .python
  stage: test
  variables:
    ENV: test
    USE_COVERAGE: $USE_COVERAGE
  script:
    - python manage.py makemigrations --settings=colmena.settings.test
    - python manage.py migrate --settings=colmena.settings.test
    - python manage.py makemessages -l en -l es -i venv -e py,html -d django --settings=colmena.settings.test
    - python manage.py compilemessages -l en -l es -i venv --settings=colmena.settings.test
    - python manage.py test --parallel --exclude-tag=ldap --exclude-tag=skip --exclude-tag=nextcloud --settings=colmena.settings.test
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  only:
    - dev
    - main
    # Match any branch name that follows our branch naming convention
    - /^\d{1,4}-.*$/
    - /^feature/.*$/
    - /^hotfix/.*$/
    - /^\d+\.\d+\.\d+$/

# ------------------------------------------------------------------------------
# Build stages
#

.publish:
  stage: publish
  # Specify a Docker image to run the job in.
  image: docker:20.10.16
  needs:
    - dev
    - test
  # Specify an additional image 'docker:dind' ("Docker-in-Docker") that
  # will start up the Docker daemon when it is brought up by a runner.
  services:
    - docker:20.10.16-dind
  before_script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login $CI_REGISTRY --username $CI_REGISTRY_USER --password-stdin
  script:
    - docker build -f $BACKEND_DOCKERFILE_PATH  -t $BACKEND_REPOSITORY_IMAGE:$RELEASE_TAG .
    - docker push $BACKEND_REPOSITORY_IMAGE:$RELEASE_TAG
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
    SECURE_LOG_LEVEL: debug
    BACKEND_REPOSITORY_IMAGE: $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/$BACKEND_REGISTRY_IMAGE_NAME

publish_dev:
  extends: .publish
  only:
    - dev
  variables:
    RELEASE_TAG: $RELEASE_TAG_DEV

publish_stage:
  extends: .publish
  only:
    - main
  variables:
    RELEASE_TAG: $RELEASE_TAG_STAGE

# ------------------------------------------------------------------------------
# Deployment stages
#

.deploy: &deploy
  stage: deploy
  image: dokku/ci-docker-image
  needs:
    - dev
    - test
  variables:
    BRANCH: "main"
    GIT_PUSH_FLAGS: "--force"
    GIT_DEPTH: 0
  script: dokku-deploy
  after_script: [dokku-unlock]

deploy_to_dev:
  extends: .deploy
  only:
    - dev
  needs:
    - publish_dev
  variables:
    GIT_REMOTE_URL: $SSH_REMOTE_DEVEL_URL
  environment:
    name: dev
    url: $DEVEL_DEPLOY_URL

deploy_to_stage:
  extends: .deploy
  only:
    - main
  needs:
    - publish_stage
  variables:
    GIT_REMOTE_URL: $SSH_REMOTE_STAGE_URL
  environment:
    name: staging
    url: $STAGE_DEPLOY_URL

# ------------------------------------------------------------------------------
# Others
#

include:
  - template: Jobs/Secret-Detection.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml

dependency_scanning:
    image: registry.gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python:2-python-3.9
    before_script:
    - apt-get -qqy update
    - apt -y install libsasl2-dev
