#!/bin/sh

SETTINGS=colmena.settings.prod
BIN=python

create_db() {
  set -e

  echo ======== Create DB ========
  $BIN ./bin/postgres.py CREATE || true
  echo
}

create_superadmin() {
  set -e
  local superadmin_email=$1
  local superadmin_password=$2
  local nc_username=$3
  local nc_password=$4

  echo ======== Create Superadmin ========
  $BIN manage.py create_superadmin \
    $superadmin_email \
    $superadmin_password \
    $nc_username \
    $nc_password
  echo
}

migrate() {
  set -e

  echo ======== Starting ecto migration ========
  $BIN manage.py makemigrations --settings=$SETTINGS
  $BIN manage.py migrate --settings=$SETTINGS
  echo
}

setup_db() {
  set -e

  # Run the create db
  create_db
  # Run the migrate script
  migrate
}

setup_seeds() {
  set -e
  local backend_hostname=$1
  local frontend_hostname=$2

  echo ======== Installing seeds ========
  $BIN manage.py load_sites_with_hostname $backend_hostname $frontend_hostname --settings=$SETTINGS
  $BIN manage.py loaddata apps/accounts/seeds/02-groups.json --settings=$SETTINGS
  $BIN manage.py setup_group_permissions --settings=$SETTINGS
  $BIN manage.py loaddata apps/accounts/seeds/04-languages.json --settings=$SETTINGS
  $BIN manage.py loaddata apps/accounts/seeds/05-regions.json --settings=$SETTINGS
}

setup_static() {
  set -e

  echo "======== Collecting static files ========"
  $bin ./manage.py collectstatic --noinput --settings=$SETTINGS

  echo "======== Compiling translations ========"
  $bin ./manage.py compilemessages -l en -l es -i venv
}

start_local() {
  set -e

  local workers=$GUNICORN_WORKERS

  echo
  echo "Starting local instance"
  echo
  echo Using settings=$SETTINGS
  echo

  # Setup static files
  setup_static

  # Run the setup_db script
  setup_db

  # Run seeds creation
  setup_seeds \
    $BACKEND_HOSTNAME \
    $FRONTEND_HOSTNAME

  # Create Superadmin User
  create_superadmin \
    $SUPERADMIN_EMAIL \
    $SUPERADMIN_PASSWORD \
    $NEXTCLOUD_ADMIN_USER \
    $NEXTCLOUD_ADMIN_PASSWORD

  echo ======== Starting Nginx ========
  service nginx start
  echo
  echo ======== Starting Local colmena ========
  gunicorn --workers $workers --bind unix:/opt/app/app.sock -m 777 colmena.wsgi:application
}

start_prod() {
  set -e

  local workers=$GUNICORN_WORKERS

  echo
  echo "Starting prod instance"
  echo
  echo Using settings=$SETTINGS
  echo

  # Setup static files
  setup_static
  # Run the setup_db script
  setup_db
  # Run seeds creation
  setup_seeds \
    $BACKEND_HOSTNAME \
    $FRONTEND_HOSTNAME

  # Create Superadmin User
  create_superadmin \
    $SUPERADMIN_EMAIL \
    $SUPERADMIN_PASSWORD \
    $NEXTCLOUD_ADMIN_USER \
    $NEXTCLOUD_ADMIN_PASSWORD

  echo ======== Starting colmena ========

  gunicorn --workers $workers colmena.wsgi:application --bind 0.0.0.0:$PORT
}

case $1 in
  create_db) "$@"; exit;;
  migrate) "$@"; exit;;
  setup_db) "$@"; exit;;
  setup_seeds) "$@"; exit;;
  start_local) "$@"; exit;;
  start_prod) "$@"; exit;;
esac
