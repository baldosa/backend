class EmailException(Exception):
    """
    Raised when an error occurs.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message=None) -> None:
        if message:
            self.message = message
        super().__init__(self.message)


class EmailDeliveryException(EmailException):
    message: str = "There was an error while delivering an email."
