from django.db.models import Q
from django.contrib.auth.models import Group
from django.http import HttpResponse

from django_countries import countries

from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema, OpenApiParameter, OpenApiExample

from rest_framework.response import Response
from rest_framework import permissions, viewsets, mixins
from rest_framework.decorators import action
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from rest_framework_simplejwt.authentication import JWTAuthentication

from apps.invitations.exceptions import UnauthorizedUserException
from apps.nextcloud.serializers import NextcloudListSharedFilesRequestSerializer
from apps.nextcloud.resources import files as nextcloud_files_utils

from apps.organizations import organizations
from apps.organizations.models import (
    Organization,
    Team,
    UserOrganizationGroupProxy,
)
from apps.organizations.exceptions import (
    OrganizationShareUploadedFileException,
    OrganizationProjectsRetrievalException,
    OrganizationProjectNotFoundException,
    OrganizationProjectDownloadException,
)
from apps.organizations.permissions import IsAdminOrOrgOwner
from apps.organizations.resources import team as organization_team_manager
from apps.organizations.resources import shares as organization_share_manager
from apps.organizations.serializers import (
    CountrySerializer,
    OrganizationSerializer,
    TeamListRequestSerializer,
    TeamListSharedFilesResponseSerializer,
    ShareUploadRequestSerializer,
    TeamRequestSerializer,
    TeamResponseSerializer,
    TeamSerializer,
    ShareProjectRequestSerializer,
    ProjectRequestSerializer,
    ProjectImportRequestSerializer,
    ProjectResponseSerializer,
    OrganizationMembersRequestSerializer,
    OrganizationMembersResponseSerializer,
)

from colmena.serializers.serializers import ErrorSerializer
from colmena.utils import encoding


class CountryViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = CountrySerializer
    permission_classes = [permissions.IsAuthenticated]
    queryset = [
        {"name": value, "code": key} for key, value in countries.countries.items()
    ]


class OrganizationViewSet(viewsets.ModelViewSet):
    queryset = organizations.list_organizations()
    serializer_class = OrganizationSerializer
    permission_classes = [permissions.IsAuthenticated]
    http_method_names = ["get", "patch"]

    def partial_update(self, request, *args, **kwargs):
        user_request = request.user
        organization_id = kwargs.get("pk")
        if not Organization.is_valid_pk(organization_id):
            return Response(ErrorSerializer("ERRORS_INVALID_REQUEST").data, status=404)
        # FIXME : Create a function in model to check this condition
        if not UserOrganizationGroupProxy.objects.filter(
            Q(
                user=user_request,
                organization=organization_id,
                group=Group.objects.get(name="OrgOwner"),
            )
            | Q(
                user=user_request,
                organization=organization_id,
                group=Group.objects.get(name="Admin"),
            )
        ).exists():
            return Response(
                ErrorSerializer("ERRORS_INVALID_PERMISSION").data, status=401
            )
        return super().partial_update(request)

    def retrieve(self, request, *args, **kwargs):
        organization = self.get_object()
        user_request = request.user
        user_organization_group = UserOrganizationGroupProxy.objects.filter(
            organization=organization
        )
        if not user_organization_group.filter(user=user_request).exists():
            return Response(
                ErrorSerializer("ERRORS_USER_IS_NOT_ORGANIZATION_MEMBER").data,
                status=403,
            )
        serializer = self.get_serializer(organization)
        return Response(serializer.data)

    @action(detail=False)
    def byuser(self, request):
        user_request = request.user
        user_organization_group = UserOrganizationGroupProxy.objects.filter(
            user=user_request
        )
        if not user_organization_group.exists():
            return Response(
                ErrorSerializer(
                    "ERRORS_NO_ORGANIZATION_FOUND_FOR_THE_CURRENT_USER"
                ).data,
                status=404,
            )

        serializer = self.get_serializer(user_organization_group.first().organization)
        return Response(serializer.data, status=200)


class TeamViewSet(
    mixins.RetrieveModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet
):
    queryset = Team.objects.all()
    serializer_class = TeamSerializer
    permission_classes = [permissions.IsAuthenticated]

    @extend_schema(
        responses=TeamResponseSerializer(many=True),
        parameters=[
            OpenApiParameter(
                name="skip_personal_workspace",
                description="Whether to return the personal workspace team or not.",
                type=bool,
                required=False,
            ),
            OpenApiParameter(
                name="last_message",
                description="Whether to add the team's latest message to the list or not.",
                type=bool,
                required=False,
            ),
        ],
        description="Returns all teams for the given user",
        examples=[
            OpenApiExample(
                "Example 1",
                summary="Teams list",
                value=[
                    {
                        "id": 7,
                        "nc_group_id": "organization-2-general",
                        "nc_conversation_token": "yxagfqh6",
                        "organization_id": 2,
                        "is_personal_workspace": False,
                        "last_message": {
                            "last_message_id": 174,
                            "last_message_text": "{actor} removed group {group}",
                            "last_message_timestamp": 1701106637,
                        },
                        "last_messages_parameters": {
                            "actor": {"type": "user", "id": "user", "name": "user"},
                            "group": {
                                "type": "group",
                                "id": "user-personal-workspace",
                                "name": "user-personal-workspace",
                            },
                        },
                    },
                    {
                        "id": 8,
                        "nc_group_id": "user-personal-workspace",
                        "nc_conversation_token": "ek6o6pes",
                        "organization_id": None,
                        "is_personal_workspace": True,
                        "last_message": {
                            "last_message_id": 1403,
                            "last_message_text": "Hello World",
                            "last_message_timestamp": 1701187609,
                        },
                        "last_messages_parameters": {},
                    },
                ],
            )
        ],
    )
    def list(self, request, *args, **kwargs):
        try:
            request_serializer = TeamListRequestSerializer(data=request.query_params)
            if request_serializer.is_valid(raise_exception=True):
                result = organization_team_manager.list(
                    request.user,
                    request_serializer.data.get("skip_personal_workspace"),
                    request_serializer.data.get("last_message"),
                )
                serializer = TeamResponseSerializer(result, many=True)
                return Response(serializer.data, status=200)
        except Exception:
            return Response(
                ErrorSerializer("ERRORS_TEAM_LIST_RETRIEVAL_FAILED").data,
                status=400,
            )

    @extend_schema(
        responses=TeamResponseSerializer(),
        parameters=[
            OpenApiParameter(
                name="last_message",
                description="Whether to add the team's latest message or not.",
                type=bool,
                required=False,
            ),
        ],
        description="Returns a single team for the given user",
        examples=[
            OpenApiExample(
                "Example 1",
                summary="Returns a single team",
                value={
                    "id": 7,
                    "nc_group_id": "organization-2-general",
                    "nc_conversation_token": "yxagfqh6",
                    "organization_id": 2,
                    "is_personal_workspace": False,
                    "last_message": {
                        "last_message_id": 174,
                        "last_message_text": "{actor} removed group {group}",
                        "last_message_timestamp": 1701106637,
                    },
                    "last_messages_parameters": {
                        "actor": {"type": "user", "id": "user", "name": "user"},
                        "group": {
                            "type": "group",
                            "id": "user-personal-workspace",
                            "name": "user-personal-workspace",
                        },
                    },
                },
            )
        ],
    )
    def retrieve(self, request, *args, **kwargs):
        try:
            team = self.get_object()
            request_serializer = TeamRequestSerializer(data=request.query_params)
            if request_serializer.is_valid(raise_exception=True):
                result = organization_team_manager.get(
                    team.id, request.user, request_serializer.data.get("last_message")
                )
                serializer = TeamResponseSerializer(result)
                return Response(serializer.data, status=200)
        except Exception:
            return Response(
                ErrorSerializer("ERRORS_TEAM_NOT_FOUND").data,
                status=404,
            )

    @extend_schema(
        responses=NextcloudListSharedFilesRequestSerializer,
        parameters=[
            OpenApiParameter(
                name="pk",
                description="The team id",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample("Team with ID 2", value="2"),
                    OpenApiExample("Team with ID 3", value="3"),
                ],
            ),
            OpenApiParameter(
                name="limit",
                description="The max number of files to return.",
                type=int,
                required=False,
                default=200,
            ),
            OpenApiParameter(
                name="descending",
                description="Whether to return items in ascending or descending order.",
                type=bool,
                required=False,
                default=True,
            ),
        ],
        description="Returns all files and media shared for the given team",
        examples=[
            OpenApiExample(
                "Example 1",
                summary="Returns a list of team details",
                value={
                    "files": [
                        {
                            "id": 1112,
                            "token": "3px44s3e",
                            "actorType": "users",
                            "actorId": "test",
                            "actorDisplayName": "test",
                            "timestamp": 1698941776,
                            "message": "{file}",
                            "messageParameters": {
                                "actor": {
                                    "type": "user",
                                    "id": "test",
                                    "name": "test",
                                },
                                "file": {
                                    "type": "file",
                                    "id": "1507",
                                    "name": "test.png",
                                    "size": 265370,
                                    "path": "test.png",
                                    "link": "https://some-url/f/1507",
                                    "etag": "7675a9eb16e72ff51ec0e9c182ec6b57",
                                    "permissions": 27,
                                    "mimetype": "image/png",
                                    "preview-available": "yes",
                                },
                            },
                            "systemMessage": "",
                            "messageType": "comment",
                            "isReplyable": True,
                            "referenceId": "2ac1ea706cdbf08f4bc48c1644575b8807e4ba7630002cec1713feab1b27d214",
                            "reactions": {},
                            "expirationTimestamp": 0,
                            "markdown": True,
                        }
                    ],
                    "quantity": 1,
                },
            )
        ],
    )
    @action(detail=False, url_path="(?P<pk>[^/.]+)/shared_files")
    def list_shared_files(self, request, *args, **kwargs):
        team = self.get_object()
        data = {
            "conversation_token": team.nc_conversation_token,
        }

        if request.query_params.get("limit", False):
            data = data | {"limit": request.query_params.get("limit")}

        if request.query_params.get("descending", False):
            data = data | {"descending": request.query_params.get("descending")}

        request_serializer = NextcloudListSharedFilesRequestSerializer(
            data=data,
        )

        if not request_serializer.is_valid():
            # TODO: Find a way to send the serializer error dictionary, in addition to the error message.
            return Response(
                ErrorSerializer("ERRORS_FILES_LIST_INVALID_REQUEST_PARAMS").data,
                status=400,
            )

        args = request_serializer.data | {"user": request.user}

        try:
            response = nextcloud_files_utils.list_conversation_files(**args)
            serializer = TeamListSharedFilesResponseSerializer(response)
            return Response(serializer.data)
        except Exception:
            return Response(
                ErrorSerializer("ERRORS_NEXTCLOUD_SERVER_ERROR").data, status=404
            )


class ShareUploadAPIView(APIView):
    parser_classes = [MultiPartParser]
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [JWTAuthentication]
    serializer_class = ShareUploadRequestSerializer

    @extend_schema(
        request=ShareUploadRequestSerializer,
        parameters=[
            OpenApiParameter(
                name="filename",
                description="A name for the file",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample(
                        "File with name hello-world.jpg", value="hello-world.jpg"
                    ),
                    OpenApiExample("File with name free.pdf", value="free.pdf"),
                ],
            ),
        ],
    )
    def post(self, request, **kwargs):
        data = {
            "filename": request.data.get("filename"),
            "file": request.data.get("file"),
            "teams": request.data.get("teams"),
        }
        request_serializer = self.serializer_class(
            data=data, context={"user_id": request.user.id}
        )

        if not request_serializer.is_valid():
            return Response(
                ErrorSerializer(
                    "ERRORS_SHARE_FILE_IN_TEAMS_INVALID_REQUEST_PARAMS"
                ).data,
                status=400,
            )

        args = list(
            {**request_serializer.validated_data, "user": request.user}.values()
        )
        try:
            organization_share_manager.upload(*args)
            return Response(None, status=200)

        except Exception:
            return Response(
                ErrorSerializer("ERRORS_SHARE_UPLOAD_FAILED").data, status=400
            )


class ProjectExportAPIView(APIView):
    parser_classes = [MultiPartParser]
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ShareProjectRequestSerializer

    @extend_schema(
        request=ShareProjectRequestSerializer,
        parameters=[
            OpenApiParameter(
                name="filename",
                description="A name for the project",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample(
                        "Project with name audio_example.zip", value="audio_example.zip"
                    ),
                    OpenApiExample(
                        "Project with name project_example.zip",
                        value="project_example.zip",
                    ),
                ],
            ),
        ],
        operation_id="export_project",
    )
    def post(self, request, **kwargs):
        data = {
            "filename": request.data.get("filename"),
            "file": request.data.get("file"),
            "teams": request.data.get("teams"),
        }
        request_serializer = self.serializer_class(
            data=data, context={"user_id": request.user.id}
        )

        if not request_serializer.is_valid():
            return Response(
                ErrorSerializer("ERRORS_SHARE_PROJECT_INVALID_REQUEST_PARAMS").data,
                status=400,
            )

        args = list(
            {**request_serializer.validated_data, "user": request.user}.values()
        )
        try:
            organization_share_manager.upload_project(*args)
            return Response("PROJECT_EXPORTED_SUCCESSFULLY", status=200)

        except OrganizationShareUploadedFileException as e:
            return Response(e.message, status=400)
        except Exception:
            return Response(
                ErrorSerializer("ERRORS_SHARE_PROJECT_FAILED").data, status=400
            )


class ProjectListAPIView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ProjectRequestSerializer

    @extend_schema(
        request=ProjectRequestSerializer,
        responses=ProjectResponseSerializer(many=True),
        description="Returns all projects for the given user",
        examples=[
            OpenApiExample(
                "List of projects",
                summary="Returns a list of projects",
                value=[
                    {
                        "id": 1333,
                        "name": "2024-04-08-10-06-26-project.zip",
                        "shared_teams": [{"team_id": 1, "name": "ExampleTeam"}],
                    },
                ],
            )
        ],
        operation_id="list_projects",
    )
    def get(self, request, *args, **kwargs):
        try:
            response = organization_share_manager.list_projects(request.user)
            return Response(response, status=200)
        except OrganizationProjectsRetrievalException as e:
            return Response(
                ErrorSerializer(e.message).data,
                status=400,
            )


class ProjectImportAPIView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ProjectImportRequestSerializer

    @extend_schema(
        request=ProjectImportRequestSerializer,
        parameters=[
            OpenApiParameter(
                name="project_id",
                description="A project id",
                location=OpenApiParameter.PATH,
                type=int,
                required=True,
                examples=[
                    OpenApiExample("File with ID 444", value=444),
                ],
            ),
        ],
        operation_id="import_project",
        responses={
            (200, "application/octet-stream"): OpenApiTypes.BINARY,
            (404, "application/json"): {"error_code": "ERRORS_PROJECT_NOT_FOUND"},
        },
    )
    def get(self, request, **kwargs):
        """
        Given a project id returns a file from Nextcloud, representing a
        project.
        """
        data = {
            "project_id": kwargs.get("project_id"),
        }
        request_serializer = ProjectImportRequestSerializer(data=data)
        if not request_serializer.is_valid():
            # TODO: Find a way to send the serializer error dictionary, in addition to the error message.
            return Response(
                ErrorSerializer("ERRORS_INVALID_IMPORT_PROJECT_REQUEST_PARAMS").data,
                status=400,
            )

        try:
            file_bytestring = organizations.download_project(
                request_serializer.data["project_id"], request.user
            )
            encoded_file = encoding.encode_file_bytestring(file_bytestring)
            return HttpResponse(
                encoded_file, content_type="application/octet-stream", status=200
            )
        except OrganizationProjectNotFoundException:
            return Response(
                ErrorSerializer("ERRORS_PROJECT_NOT_FOUND").data, status=404
            )

        except OrganizationProjectDownloadException:
            return Response(
                ErrorSerializer(
                    "ERRORS_UNEXPECTED_ERROR_WHILE_DOWNLOADING_PROJECT"
                ).data,
                status=400,
            )


class OrganizationMembersView(APIView):
    """
    The OrganizationMembersView defines the operation to get the members of an organisation.
    """

    serializer_class = OrganizationMembersRequestSerializer
    permission_classes = [IsAuthenticated, IsAdminOrOrgOwner]

    @extend_schema(
        operation_id="get_organization_members",
        request=OrganizationMembersRequestSerializer,
        responses={
            200: OrganizationMembersResponseSerializer,
            401: {"error_code": "ERRORS_UNAUTHORIZED_USER"},
        },
        parameters=[
            OpenApiParameter(
                name="organization_id",
                description="The organization Id",
                location=OpenApiParameter.PATH,
                type=int,
                required=True,
            ),
        ],
    )
    def get(self, request, **kwargs):
        data = {"organization_id": kwargs.get("organization_id")}
        serializer = self.serializer_class(data=data)

        try:
            serializer.is_valid(raise_exception=True)
            active_members, suspended_members = organizations.get_organization_members(
                serializer.data.get("organization_id"), request.user
            )

            members_data = {
                "active": {"users": active_members},
                "suspended": {"users": suspended_members},
            }

            return Response(
                data=OrganizationMembersResponseSerializer(members_data).data,
                status=200,
            )
        except UnauthorizedUserException:
            return Response(
                data=ErrorSerializer("ERRORS_UNAUTHORIZED_USER").data, status=401
            )
