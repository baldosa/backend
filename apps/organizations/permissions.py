from rest_framework.permissions import BasePermission
from apps.organizations.organizations import user_has_any_role_in_organization


class IsAdminOrOrgOwner(BasePermission):
    """
    Allows access only to Admin or OrgOwner users in the given organization.
    """

    def has_permission(self, request, view):
        """
        Return `True` if user is Admin or OrgOwner in the given organization
        """
        return user_has_any_role_in_organization(
            request.user, ["Admin", "OrgOwner"], view.kwargs["organization_id"]
        )
