from datetime import datetime

from django.contrib.auth.models import Group
from drf_extra_fields.fields import Base64ImageField
from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers

from apps.accounts.models import Language, User
from apps.organizations.models import (
    Organization,
    UserOrganizationGroup,
    Team,
    UserTeam,
)
from colmena.validators import files as files_validator


class CountrySerializer(serializers.Serializer):
    name = serializers.CharField()
    iso_code = serializers.CharField(source="code", required=False)


# ------------------------------------------------------------------------------
# Organization serializers
#


class OrganizationLanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = "__all__"


class OrganizationMemberGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = (
            "id",
            "name",
        )


class OrganizationMemberSerializer(serializers.ModelSerializer):
    avatar = Base64ImageField(required=False, represent_in_base64=True)

    class Meta:
        model = User
        fields = (
            "pk",
            "username",
            "full_name",
            "avatar",
            "email",
        )


class UserOrganizationGroupSerializer(serializers.ModelSerializer):
    group = OrganizationMemberGroupSerializer()
    user = OrganizationMemberSerializer()

    class Meta:
        model = UserOrganizationGroup
        fields = (
            "group",
            "user",
            "created_at",
            "updated_at",
        )

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["user"] = OrganizationMemberSerializer(instance.user).data
        representation["group"] = OrganizationMemberGroupSerializer(instance.group).data
        return representation


class OrganizationSerializer(serializers.ModelSerializer):
    logo = Base64ImageField(required=False, represent_in_base64=True)

    class Meta:
        model = Organization
        fields = (
            "id",
            "name",
            "email",
            "website",
            "country",
            "logo",
            "additional_info",
            "created_by",
            "language",
        )
        extra_kwargs = {
            "website": {"error_messages": {"invalid": "ERRORS_INVALID_WEBSITE_FORMAT"}},
            "country": {"error_messages": {"invalid_choice": "ERRORS_INVALID_COUNTRY"}},
            "language": {
                "error_messages": {"does_not_exist": "ERRORS_INVALID_LANGUAGE"}
            },
        }

    def validate_logo(self, value):
        if value:
            files_validator.validate_max_size(value)
            return value

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["country"] = CountrySerializer(instance.country).data
        representation["language"] = OrganizationLanguageSerializer(
            instance.language
        ).data
        return representation


class OrganizationMembersRequestSerializer(serializers.Serializer):
    organization_id = serializers.IntegerField(required=True)


class OrganizationMembersDataSerializer(serializers.Serializer):
    users = UserOrganizationGroupSerializer(many=True)
    amount = serializers.SerializerMethodField()

    @extend_schema_field(serializers.IntegerField)
    def get_amount(self, instance):
        return len(instance["users"])

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["amount"] = self.get_amount(instance)
        return representation


class OrganizationMembersResponseSerializer(serializers.Serializer):
    active = OrganizationMembersDataSerializer()
    suspended = OrganizationMembersDataSerializer()


# ------------------------------------------------------------------------------
# Team serializers
#


class TeamSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    organization_id = serializers.PrimaryKeyRelatedField(
        read_only=True, allow_null=True
    )
    organization_name = serializers.CharField(
        source="organization.name", allow_null=True
    )

    class Meta:
        model = Team
        fields = (
            "id",
            "name",
            "nc_group_id",
            "nc_conversation_token",
            "organization_id",
            "organization_name",
            "is_personal_workspace",
        )


class TeamRequestSerializer(serializers.Serializer):
    last_message = serializers.BooleanField(required=False, default=False)


class TeamListRequestSerializer(TeamRequestSerializer):
    skip_personal_workspace = serializers.BooleanField(required=False, default=False)


class TeamResponseSerializer(TeamSerializer):
    organization_id = serializers.IntegerField(allow_null=True)
    last_message = serializers.DictField(required=False)
    organization_name = serializers.CharField()
    members_count = serializers.SerializerMethodField()

    @extend_schema_field(field=serializers.IntegerField)
    def get_members_count(self, instance):
        if instance["is_personal_workspace"]:
            return 1
        return UserTeam.objects.filter(team__id=instance["id"]).count()

    class Meta(TeamSerializer.Meta):
        fields = TeamSerializer.Meta.fields + (
            "last_message",
            "members_count",
        )


class TeamListSharedFilesResponseSerializer(serializers.Serializer):
    files = serializers.ListField(child=serializers.DictField())

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["quantity"] = len(representation["files"])
        return representation


# ------------------------------------------------------------------------------
# Share serializers
#


class ShareUploadRequestSerializer(serializers.Serializer):
    file = serializers.FileField(required=True)
    filename = serializers.CharField(required=True)
    teams = serializers.CharField()

    def validate_filename(self, value):
        _, extension = files_validator.get_filename_extension(value)
        if files_validator.is_valid_extension(extension):
            return datetime.now().strftime("%Y-%m-%d-%H-%M-%S-") + value
        else:
            raise serializers.ValidationError("ERRORS_EXTENSION_IS_NOT_VALID")

    def validate_teams(self, value):
        try:
            teams_ids = value.split(",")
            teams = Team.objects.filter(
                userteam__user_id=self.context.get("user_id"),
                userteam__team_id__in=teams_ids,
            )
            assert len(teams_ids) == teams.count()
            return teams
        except Exception as e:
            raise serializers.ValidationError("ERRORS_TEAMS_SELECTION_IS_NOT_VALID")


class ShareProjectRequestSerializer(ShareUploadRequestSerializer):
    def validate_filename(self, value):
        _, extension = files_validator.get_filename_extension(value)
        if files_validator.is_project_extension(extension):
            return datetime.now().strftime("%Y-%m-%d-%H-%M-%S-") + value
        else:
            raise serializers.ValidationError("ERRORS_EXTENSION_IS_NOT_VALID")


class ProjectRequestSerializer(serializers.Serializer):
    pass


class ProjectImportRequestSerializer(serializers.Serializer):
    project_id = serializers.IntegerField(required=True)


class ProjectSharedTeamSerializer(serializers.Serializer):
    team_id = serializers.IntegerField(required=True)
    name = serializers.CharField()
    is_personal_workspace = serializers.BooleanField()


class ProjectResponseSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=True)
    name = serializers.CharField()
    shared_teams = ProjectSharedTeamSerializer(many=True)
