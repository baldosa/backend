class OrganizationException(Exception):
    """
    Raised when the app password could not be created.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message=None):
        if message:
            self.message = message
        super().__init__(self.message)


class OrganizationTeamCreationException(OrganizationException):
    message: str = "The organization team could not be created."


class OrganizationAddTeamMemberException(OrganizationException):
    message: str = "The user could not added to the team."


class OrganizationRemoveTeamMemberException(OrganizationException):
    message: str = "The user could not be removed form the team."


class OrganizationPersonalWorkspaceCreationException(OrganizationException):
    message: str = "The personal workspace could not be created."


class OrganizationTeamDeletionException(OrganizationException):
    message: str = "The team could not be deleted."


class OrganizationCantInviteMembersException(OrganizationException):
    message: str = "The user does not have permissions to invite members."


class OrganizationShareUploadedFileException(OrganizationException):
    message: str = "The user could not upload the file to the team"


class OrganizationProjectsRetrievalException(OrganizationException):
    message: str = "The user projects could not be retrieved"


class OrganizationProjectNotFoundException(OrganizationException):
    """
    Raised when a project could not be retrieved.
    """

    message: str = "The project could not be found"


class OrganizationProjectDownloadException(OrganizationException):
    """
    Raised when a project download could not be performed.
    """

    message: str = "The project could not be downlaoded"
