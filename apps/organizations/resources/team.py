from apps.accounts.models import get_super_admin, User

from apps.nextcloud import nextcloud as nextcloud_manager
from apps.nextcloud.exceptions import (
    NextcloudGroupCreationException,
    NextcloudConversationCreationException,
)
from apps.nextcloud.resources import groups as nextcloud_group_manager
from apps.nextcloud.resources import talk as nextcloud_talk_manager
from apps.nextcloud.resources import users as nextcloud_user_manager
from apps.nextcloud.serializers import NextcloudLastConversationMessageSerializer

from apps.organizations.exceptions import (
    OrganizationAddTeamMemberException,
    OrganizationRemoveTeamMemberException,
    OrganizationTeamCreationException,
    OrganizationTeamDeletionException,
)
from apps.organizations.exceptions import OrganizationPersonalWorkspaceCreationException
from apps.organizations.models import Team, UserTeam
from apps.organizations.serializers import TeamSerializer

from colmena.serializers.serializers import ErrorSerializer

from django.core.exceptions import ValidationError
from django.contrib.auth.models import Group


def create(group_name, nextcloud_user_id, organization, team_name, user):
    """
    Given a group name, a nextcloud user id, an organization id and a user, creates a nextcloud
    group and a conversation and adds the user to them. Finally creates an Team model instance
    and associates it to the given organization id.

    Parameters
    ----------
    group_name : str
        The group name

    nextcloud_user_id : str
        The nextcloud user id

    organization : Organization
        The organization model.

    team_name : str
        The team name

    user : User
        The user model.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        When any nextcloud operation fails
    """
    # Setup
    superadmin_user = get_super_admin()
    try:
        # Create a nextcloud group
        nextcloud_group_manager.create_group(group_id=group_name, user=superadmin_user)

        # Initialise conversation
        nc_conversation_token = nextcloud_manager.initialise_conversation(
            group_name, nextcloud_user_id, user, superadmin_user
        )

        # Create a new team
        team = Team.objects.create(
            nc_group_id=group_name,
            nc_conversation_token=nc_conversation_token,
            organization=organization,
            name=team_name,
        )

        # Create a UserTeam
        UserTeam.objects.create(user=user, team=team)

    except Exception as e:
        # Remove a group
        if nextcloud_group_manager.get_group(group_id=group_name, user=superadmin_user):
            nextcloud_group_manager.remove_group(
                group_id=group_name, user=superadmin_user
            )
        nextcloud_user_manager.delete_user(
            user_id=user.username,
            auth_user_id=superadmin_user.get_nextcloud_user_id(),
            auth_user_app_password=superadmin_user.nc_app_password,
        )

        # Remove a team if was created
        try:
            team.delete()
        except UnboundLocalError:
            pass

        raise OrganizationTeamCreationException


def add_member(
    user: User,
    group: Group,
    nextcloud_user_id: str,
    team: Team,
    invited_by: User,
    superadmin_user: User,
):
    """
    Given a user, a nextcloud user id, a team, an organization owner user and a superadmin user to authenticate the request, adds a user to the given team.


    Parameters
    ----------
    user : User
        The user model

    group : Group
        The group model assigned to the member

    nextcloud_user_id : str
        The nextcloud user id

    team : Team
        The team model

    invited_by : User
        The user model. This user is assumed to have the required
        privileges to add users to a nextcloud group.

    superadmin_user : User
        The user model. This user is assumed to have the required
        privileges to promote users to subadmin.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        If the any operation could not be performed in nextcloud
    """
    try:
        # Add user to group
        nextcloud_group_manager.add_user_to_group(
            group_id=team.nc_group_id,
            user_id=nextcloud_user_id,
            user=invited_by,
        )

        # Add a User Team
        UserTeam.objects.create(user=user, team=team)

        if group.name == "Admin":
            # Provides conversation creation privileges to the Admin
            nextcloud_group_manager.promote_user_to_subadmin(
                group_id=team.nc_group_id,
                user_id=nextcloud_user_id,
                user=superadmin_user,
            )
    except Exception as e:
        nextcloud_user_manager.delete_user(
            user_id=user.get_nextcloud_user_id(),
            auth_user_id=superadmin_user.get_nextcloud_user_id(),
            auth_user_app_password=superadmin_user.nc_app_password,
        )
        raise OrganizationAddTeamMemberException


def create_personal_workspace(
    user: User,
    nextcloud_user_id: str,
):
    """
    Given a user and a nextcloud user id, creates a Team and a conversation in nextcloud for the given user.

    Parameters
    ----------
    user : User
        The user model

    nextcloud_user_id : str
        The nextcloud user id

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        When any nextcloud operation fails
    """
    # Setup
    superadmin_user = get_super_admin()
    group_name = nextcloud_group_manager.build_personal_workspace_name(
        nextcloud_user_id
    )

    try:
        # Create a nextcloud personal group
        nextcloud_group_manager.create_group(group_id=group_name, user=superadmin_user)

        # Initialise conversation
        nc_conversation_token = nextcloud_manager.initialise_conversation(
            group_name, nextcloud_user_id, user, superadmin_user
        )

        # Create a new team
        team = Team.objects.create(
            nc_group_id=group_name,
            nc_conversation_token=nc_conversation_token,
            is_personal_workspace=True,
        )

        # Create a UserTeam
        UserTeam.objects.create(user=user, team=team)

    except NextcloudGroupCreationException:
        # TODO: add error logs
        _rollback_user_creation(user, superadmin_user)
        raise OrganizationPersonalWorkspaceCreationException

    except NextcloudConversationCreationException:
        # TODO: add error logs
        _rollback_user_creation(user, superadmin_user)
        raise OrganizationPersonalWorkspaceCreationException

    except Exception as e:
        _rollback_user_creation(user, superadmin_user)
        # TODO: add error logs
        raise NextcloudGroupCreationException


def _rollback_user_creation(user, superadmin_user):
    nextcloud_user_manager.delete_user(
        user_id=user.username,
        auth_user_id=superadmin_user.get_nextcloud_user_id(),
        auth_user_app_password=superadmin_user.nc_app_password,
    )


def delete(team, auth_user):
    """
    Given a team and a team admin user to authenticate the request,
    wipes out the nextcloud group and removes the team.

    Parameters
    ----------
    team : Team
        The team model

    auth_user : User
        A user model. This user is assumed to have the required
        privileges in the team to perform nextcloud requests with an app password.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        If the group deletion fails.
    """
    try:
        nextcloud_group_manager.remove_group(
            group_id=team.nc_group_id,
            user=auth_user,
        )
    except Exception as e:
        # TODO: add logs
        raise OrganizationTeamDeletionException


def remove_from_team(user, team, team_admin):
    """
    Given a user, a team and a admin team user to authenticate the request,
    removes the user from the given team.

    Parameters
    ----------
    user : User
        The user model

    team : Team
        The team model

    team_admin : User
        A user model. This user is assumed to have the required
        privileges in the team to perform nextcloud requests with an app password.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        If the user removal fails.
    """
    try:
        nextcloud_group_manager.remove_user_from_group(
            group_id=team.nc_group_id,
            user_id=user.get_nextcloud_user_id(),
            user=team_admin,
        )
        user_team = UserTeam.objects.get(user=user, team=team)
        user_team.delete()
    except Exception as e:
        raise OrganizationRemoveTeamMemberException


def delete_personal_workspace(team, auth_user):
    """
    Given a personal workspace team and superadmin user to authenticate the request,
    removes the personal workspace with the given user.

    Parameters
    ----------
    team : Team
        The team model

    auth_user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        If the personal workspace deletion fails.
    """
    try:
        delete(team, auth_user)
    except Exception as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_PERSONAL_WORKSPACE_DELETION_FAILED").data
        )


def list(auth_user, skip_personal_workspace, last_message):
    """
    Given a user to authenticate the request,
    list a user's teams.

    Parameters
    ----------
    auth_user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    skip_personal_workspace : Bool
        Whether to add the personal workspace to the teams list.

    last_message : Bool
        Wheter to add the team's latest message to the teams list.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        If the list teams fails.
    """
    try:
        teams = Team.objects.filter(userteam__user_id=auth_user.id)
        if skip_personal_workspace:
            teams = teams.filter(is_personal_workspace=False)

        team_serializer = TeamSerializer(teams, many=True)
        serialized_team = team_serializer.data
        result = serialized_team
        if last_message:
            nc_conversations = nextcloud_talk_manager.get_user_conversations(
                user=auth_user
            )
            conversation_serializer = NextcloudLastConversationMessageSerializer(
                nc_conversations, many=True
            )

            serialized_conversations = conversation_serializer.data

            last_message_map = {
                conversation["conversation_token"]: conversation["last_message"]
                for conversation in serialized_conversations
            }
            result = [
                {
                    **team,
                    "last_message": last_message_map.get(team["nc_conversation_token"]),
                }
                for team in serialized_team
            ]
        return result
    except Exception as e:
        raise ValidationError("ERRORS_GET_TEAM_LIST_FAILED")


def get(team_id, auth_user, last_message):
    """
    Given a team_id and user to authenticate the request,
    returns a single team.


    Parameters
    ----------
    team_id : int
        The team id

    auth_user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    last_message : Bool
        Whether to add the team's latest message.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        If the get a team fails.

    """

    try:
        queryset = Team.objects.filter(userteam__user_id=auth_user.id)
        team = queryset.get(id=team_id)
        team_serializer = TeamSerializer(team)
        result = team_serializer.data
        if last_message:
            nc_conversation = nextcloud_talk_manager.get_conversation(
                conversation_token=team.nc_conversation_token, user=auth_user
            )
            conversation_serializer = NextcloudLastConversationMessageSerializer(
                nc_conversation
            )

            result = {
                **team_serializer.data,
                "last_message": conversation_serializer.data.get("last_message"),
            }

        return result
    except Exception as e:
        raise ValidationError("ERRORS_GET_TEAM_FAILED")


def get_team_by_nc_group_id(team_name: str):
    return Team.objects.get(nc_group_id=team_name)
