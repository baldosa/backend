"""
This module provides an API to interact with specific functions of the
organizations API, such as retrieving entities from a model, interacting with
organizations APIs or even perform validations.
This module is considered the entrypoint to the organizations bussiness model.
"""

import logging

from django.apps import apps
from django.contrib.auth.models import Group
from django.core.exceptions import ValidationError
from django.db.models import Q

from apps.accounts import accounts
from apps.invitations.exceptions import UnauthorizedUserException
from apps.accounts.models import OrganizationMemberInvitation, User, get_super_admin
from apps.nextcloud.resources import groups as nextcloud_group_manager
from apps.nextcloud.resources import users as nextcloud_user_manager

from .exceptions import (
    OrganizationAddTeamMemberException,
    OrganizationCantInviteMembersException,
)

from .models import (
    Organization,
    Team,
    UserOrganizationGroupProxy,
    UserOrganizationGroup,
)
from .resources import team as organization_team_manager
from .resources import shares as shares_manager

logger = logging.getLogger(__name__)


# ------------------------------------------------------------------------------
# Model accesors
#


def list_organizations() -> Organization:
    return Organization.objects.all()


def get_organization(organization_id: int) -> Organization:
    return Organization.objects.get(id=organization_id)


def _organization_exist(organization_id: int) -> Organization:
    return Organization.objects.filter(id=organization_id).exists()


def create_organization_member(
    username: str, fullname: str, email: str, organization: Organization, group_id: int
) -> tuple[OrganizationMemberInvitation, Organization, Group]:
    group: Group = Group.objects.get(id=group_id)

    organization_member: OrganizationMemberInvitation = (
        accounts.create_organization_member_invitation(
            username=username, fullname=fullname, email=email
        )
    )
    organization_member.language.add(organization.language)
    organization_member.groups.add(group)
    create_user_organization_group(
        member=organization_member, organization=organization, group=group
    )

    return organization_member, organization, group


def create_user_organization_group(
    member: OrganizationMemberInvitation, organization: Organization, group: Group
) -> UserOrganizationGroupProxy:
    return UserOrganizationGroupProxy.objects.create(
        user=member,
        organization=organization,
        group=group,
    )


def validate_invitation_permission(user: User, organization: Organization) -> bool:
    if not UserOrganizationGroupProxy.objects.filter(
        Q(
            user=user,
            organization=organization.id,
            group=Group.objects.get(name="Admin"),
        )
        | Q(
            user=user,
            organization=organization.id,
            group=Group.objects.get(name="OrgOwner"),
        )
    ).exists():
        raise OrganizationCantInviteMembersException


def user_exists_in_organization(user_id):
    UserOrganizationGroupProxy = apps.get_model(
        "organizations", "UserOrganizationGroupProxy"
    )
    return UserOrganizationGroupProxy.objects.filter(user__id=user_id).exists()


def get_personal_workspace(nextcloud_user_id):
    Team = apps.get_model("organizations", "Team")

    user = accounts.get_user(nextcloud_user_id)

    try:
        return Team.objects.get(userteam__user_id=user.id, is_personal_workspace=True)
    except Exception as e:
        raise ValidationError("USER_GET_PERSONAL_WORKSPACE_TEAM_FAILED") from e


def initialise_groups(
    user: User,
    organization: Organization,
    group: Group,
    nextcloud_user_id: str,
    invited_by: User,
):
    group_name: str = nextcloud_group_manager.build_group_name(
        org_id=organization.id,
        team_identifier=nextcloud_group_manager.GENERAL_GROUP_NAME,
    )

    match group:
        case Group(name="OrgOwner"):
            organization_team_manager.create(
                group_name,
                nextcloud_user_id,
                organization,
                organization.name,
                user,
            )

        case Group(name="Admin") | Group(name="User"):
            try:
                team: Team = Team.objects.get(nc_group_id=group_name)
                organization_team_manager.add_member(
                    user=user,
                    group=group,
                    nextcloud_user_id=nextcloud_user_id,
                    team=team,
                    invited_by=invited_by,
                    superadmin_user=get_super_admin(),
                )

            except OrganizationAddTeamMemberException as e:
                superadmin_user = get_super_admin()
                nextcloud_user_manager.delete_user(
                    user_id=nextcloud_user_id,
                    auth_user_id=superadmin_user.get_nextcloud_user_id(),
                    auth_user_app_password=superadmin_user.nc_app_password,
                )
                raise OrganizationAddTeamMemberException from e

    organization_team_manager.create_personal_workspace(user, nextcloud_user_id)


def download_project(file_id: int, user: User) -> bytes | bytearray | memoryview:
    """
    Given an organisation identifier, returns the members of an organisation grouped into active and suspended.
    """
    return shares_manager.download_project(file_id, user)


def user_has_any_role_in_organization(
    user: User, roles: list, organization_id: int
) -> bool:
    return UserOrganizationGroup.objects.filter(
        group__name__in=roles, organization_id=organization_id, user_id=user.pk
    ).exists()


def get_organization_members(organization_id: int, user: User) -> tuple[list, list]:
    """
    Given an organization id, returns a tuple where the first element is a list
    of active users, and the second element is a list of suspended users.
    The suspended feature is currently not implemented, thus an empty list is
    assigned.
    """

    if not user_has_any_role_in_organization(
        user, ["Admin", "OrgOwner"], organization_id
    ):
        raise UnauthorizedUserException

    organization_members: list = UserOrganizationGroup.objects.filter(
        organization_id=organization_id
    )

    # FIXME: When you have the functionality to suspend users, you must check the suspension status.
    query = {"user__nc_app_password": ""}
    active_members: list = organization_members.exclude(**query)
    # FIXME: When there is a functionality to suspend users, it will be replaced by the list of suspended users.
    suspended_members: list = []

    return active_members, suspended_members
