import logging

from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.core.exceptions import ValidationError

from apps.organizations.models import Team
from apps.organizations.resources.team import delete_personal_workspace
from apps.accounts.models import get_super_admin

logger = logging.getLogger(__name__)


@receiver(pre_delete, sender=Team)
def remove_team_data_in_nextcloud(sender, instance, **kwargs) -> None:
    logger.debug(
        "Deleting team on nextcloud",
        extra={"sender": sender, "team_id": instance.id},
    )
    _handle_pre_delete(instance)


def _handle_pre_delete(instance: Team):
    try:
        superadmin = get_super_admin()
        if instance.is_personal_workspace:
            delete_personal_workspace(team=instance, auth_user=superadmin)
        # TODO:Add code for the nextcloud deletion logic for team types other than personal workspace.
    except Exception as e:
        logger.error(
            "Failed deleting a team",
            extra={
                "team_id": instance.id,
                "auth_user_id": superadmin.get_nextcloud_user_id(),
            },
        )
        raise ValidationError("ERRORS_NEXTCLOUD_DELETE_USER")
