from django.core.exceptions import ValidationError


class InvalidPasswordException(ValidationError):
    status_code = 400
    reason = "The password is not valid."

    def __init__(self):
        """Configure exception."""
        super(ValidationError, self).__init__()
