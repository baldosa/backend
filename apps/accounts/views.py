import logging

from rest_framework import generics, viewsets, status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.views import TokenRefreshView


from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.models import Group
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.db.models import Q
from django.http import Http404
from django.shortcuts import render
from django.utils.translation import gettext_lazy
from django.views.generic.edit import UpdateView
from apps.invitations import invitations
from apps.accounts.common import is_superadmin_or_staff
from apps.accounts.models import (
    User,
    UserInvitation,
    Language,
)
from apps.accounts.serializers import (
    GroupSerializer,
    LanguageSerializer,
    PasswordResetConfirmSerializer,
    PasswordResetSerializer,
    UserSerializer,
)
from apps.nextcloud.exceptions import (
    NextcloudPasswordCreationException,
    NextcloudUserNotFound,
)
from apps.accounts.exceptions import InvalidPasswordException
from apps.accounts.form import PasswordResetForm, UserInvitationForm
from dj_rest_auth.views import (
    PasswordResetView as _PasswordResetView,
    PasswordResetConfirmView as _PasswordResetConfirmView,
)
from django.contrib.auth.views import (
    PasswordResetView as CorePasswordResetView,
    PasswordResetConfirmView as CorePasswordResetConfirmView,
)

from colmena.serializers.serializers import (
    ErrorSerializer,
    ColmenaTokenRefreshSerializer,
)

from apps.nextcloud.occ import AppPasswordException

from drf_spectacular.utils import extend_schema

logger = logging.getLogger(__name__)

# -----------------------------------------------------------------------------
# Create your views here.
#


class GroupViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Group.objects.filter(Q(name="User") | Q(name="Admin"))
    serializer_class = GroupSerializer


class LanguageViewSet(viewsets.ReadOnlyModelViewSet):
    """
    A Language viewset
    """

    queryset = Language.objects.all()
    serializer_class = LanguageSerializer


class UserViewSet(viewsets.ModelViewSet, generics.UpdateAPIView):
    queryset = User.objects.all().order_by("-created_at")
    serializer_class = UserSerializer
    filter_fields = ("username",)
    http_method_names = ["get", "patch"]

    def retrieve(self, request, *args, **kwargs):
        pk = kwargs.get("pk")
        if not User.objects.filter(pk=pk).exists():
            return Response(ErrorSerializer("ERRORS_USER_NOT_FOUND").data, status=404)
        return super(UserViewSet, self).retrieve(request, pk=pk)

    def update(self, request, *args, **kwargs):
        user = User.objects.get(email=request.user.email)
        self.object = self.get_object()
        if user.id != self.object.id:
            return Response(
                ErrorSerializer("ERRORS_INVALID_OPERATION").data, status=401
            )
        # Extract data to request
        data = request.data

        # Check if request.data is valid
        if (
            not (set(data.keys()).issubset(set(UserSerializer().get_fields().keys())))
            or not data.keys()
        ):
            return Response(
                ErrorSerializer("ERRORS_INVALID_REQUEST_DATA").data, status=400
            )

        # Remove email to data if same to email user request
        if data.get("email") == request.user.email:
            data.pop("email")

        # use the partial flag only to validate the given fields.
        serializer = self.get_serializer(data=request.data, partial=True)

        if serializer.is_valid():
            serializer.update(user, serializer.validated_data)
            return Response(UserSerializer(user).data, status=200)
        else:
            return Response(serializer.errors, status=400)


class UserInvitationUpdateView(UpdateView):
    model = UserInvitation
    success_url = "/admin/"
    form_class = UserInvitationForm
    template_name = "accounts/userinvitation_form.html"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        return kwargs

    def get_object(self, queryset=None):
        encoded_invitation_id = self.kwargs.get("invitation_id")
        invitation = invitations.validate_invitation(
            encoded_invitation_id=encoded_invitation_id,
            invitation_type="administration",
        )
        user = invitation.user
        return user

    def form_valid(self, form):
        try:
            response = super().form_valid(form)
            # HINT: The login function is used to authenticate the user and redirect
            # to the initial screen with the user already logged in.
            login(self.request, self.object)
            return response
        except InvalidPasswordException:
            logger.error(
                "The provided password is very common, aborting user initialisation"
            )
            messages.error(
                self.request,
                gettext_lazy("The password is too common, please provide a new one."),
            )
        except NextcloudPasswordCreationException:
            logger.error("Unexpected error while creation nextcloud app password")
            messages.error(
                self.request,
                gettext_lazy(
                    "There was an error processing the request, please try again later."
                ),
            )
        except (NextcloudUserNotFound, AppPasswordException):
            logger.error("Unexpected error while checking new user creation")
            messages.error(
                self.request,
                gettext_lazy(
                    "There was an error processing the request, please try again later."
                ),
            )
        except Exception:
            logger.error("Unexpected error")
            messages.error(
                self.request,
                gettext_lazy("Unexpected error. please try again later"),
            )
        return self.form_invalid(form)


class PasswordResetView(_PasswordResetView):
    serializer_class = PasswordResetSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(
                "PASSWORD_RESET_REQUEST_RECEIVED",
                status=status.HTTP_200_OK,
            )
        else:
            error_message = serializer.errors["email"][0]
            return Response(error_message, status=status.HTTP_200_OK)


class PasswordResetConfirmView(_PasswordResetConfirmView):
    serializer_class = PasswordResetConfirmSerializer
    permission_classes = (AllowAny,)

    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(
                "PASSWORD_RESET_CONFIRMATION_SUCCESS",
                status=status.HTTP_200_OK,
            )
        else:
            error_message = serializer.errors["non_field_errors"][0]
            return Response(error_message, status=status.HTTP_400_BAD_REQUEST)


class AdminPasswordResetView(CorePasswordResetView):
    permission_classes = (AllowAny,)
    form_class = PasswordResetForm
    token_generator = PasswordResetTokenGenerator
    template_name = "accounts/passwordreset_form.html"
    title = gettext_lazy("Password Reset")

    def renderPasswordResetDone(self, request, context):
        return render(
            request,
            "accounts/passwordreset_done.html",
            context,
            status=status.HTTP_200_OK,
        )

    def form_valid(self, form):
        request = self.request
        context = self.get_context_data(form=form)
        try:
            form.save()
            # builds context for displaying email
            context["email_address"] = form.cleaned_data.get("email")
        except User.DoesNotExist:
            context["email_address"] = form.data["email"]
        return self.renderPasswordResetDone(request, context)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        return self.form_valid(form)


class AdminPasswordResetConfirmView(CorePasswordResetConfirmView):
    permission_classes = (AllowAny,)
    post_reset_login = False
    post_reset_login_backend = None
    success_url = None
    template_name = "accounts/passwordresetconfirm_form.html"
    title = gettext_lazy("Password Reset")
    token_generator = PasswordResetTokenGenerator()

    def dispatch(self, request, *args, **kwargs):
        self.user = self.get_user(kwargs["uid"])
        if not is_superadmin_or_staff(self.user):
            raise Http404(gettext_lazy("Invalid Token."))
        if self.user is not None and self.token_generator.check_token(
            self.user, kwargs["token"]
        ):
            self.validlink = True
            return self.post(request, *args, **kwargs)
        else:
            raise Http404(gettext_lazy("Your URL may have expired."))

    def form_valid(self, form):
        form.save()
        return render(
            self.request,
            "accounts/passwordresetconfirm_complete.html",
            context=None,
            status=status.HTTP_200_OK,
        )


@extend_schema(
    request=ColmenaTokenRefreshSerializer,
    responses={200: {"type": "object", "properties": {"access": {"type": "string"}}}},
)
class ColmenaTokenRefreshView(TokenRefreshView):
    pass
