from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import UserManager
from django.utils import timezone
from django.contrib.auth.models import PermissionsMixin, Group
from django.utils.translation import gettext_lazy
from django.db.models.functions import Lower
from django.core.exceptions import ValidationError
from colmena.validators.files import validate_max_size
from colmena.validators.validators import ValidatorOnlyLettersAndSpaces
from colmena.settings.base import SUPERADMIN_EMAIL, USER_AVATARS_PATH


class Region(models.Model):
    name = models.CharField(
        gettext_lazy("region_prop_name"), max_length=50, unique=True
    )

    class Meta:
        verbose_name = gettext_lazy("region_model_name")
        verbose_name_plural = gettext_lazy("region_model_plural_name")

    def __str__(self):
        return self.name


class Language(models.Model):
    name = models.CharField(
        gettext_lazy("languages_prop_name"), max_length=30, unique=True
    )
    iso_code = models.CharField(
        gettext_lazy("languages_prop_iso_code"), max_length=5, unique=True
    )

    def __str__(self):
        return self.name

    class Meta:
        db_table = "languages"
        verbose_name = gettext_lazy("languages_model_name")
        verbose_name_plural = gettext_lazy("languages_model_plural_name")


class User(AbstractBaseUser, PermissionsMixin):
    full_name = models.CharField(
        gettext_lazy("users_prop_full_name"),
        max_length=80,
        default="",
        validators=[ValidatorOnlyLettersAndSpaces()],
    )
    language = models.ManyToManyField(
        Language, verbose_name=gettext_lazy("languages_model_name")
    )
    region = models.ForeignKey(
        Region,
        on_delete=models.RESTRICT,
        verbose_name=gettext_lazy("region_model_name"),
        null=True,
    )
    username = models.CharField(
        gettext_lazy("users_prop_username"),
        max_length=30,
        unique=True,
    )
    avatar = models.ImageField(
        upload_to=USER_AVATARS_PATH,
        null=True,
        blank=True,
        validators=[validate_max_size],
    )
    email = models.EmailField(
        gettext_lazy("users_prop_email_address"), max_length=80, unique=True
    )
    nc_app_password = models.CharField(
        gettext_lazy("users_prop_nc_app_password"), max_length=128, blank=True
    )
    is_active = models.BooleanField(
        gettext_lazy("users_prop_is_active"),
        default=False,
        help_text=(
            gettext_lazy(
                "Designates whether this user should be treated as active. Unselect this instead of deleting users."
            )
        ),
    )
    is_staff = models.BooleanField(
        gettext_lazy("users_prop_is_staff_status"),
        default=False,
        help_text=gettext_lazy(
            "Designates whether the user can log into this admin site."
        ),
    )
    is_superuser = models.BooleanField(
        gettext_lazy("users_prop_is_super_user"),
        default=False,
    )
    created_at = models.DateTimeField(
        gettext_lazy("created_at"), default=timezone.now, editable=False
    )
    updated_at = models.DateTimeField(
        gettext_lazy("updated_at"), default=timezone.now, editable=False
    )

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = (
        "email",
        "full_name",
    )

    objects = UserManager()

    def save(self, *args, **kwargs):
        """On save, update timestamps"""
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        super(User, self).save(*args, **kwargs)

    def __str__(self):
        return self.get_username()

    def belongs_to_group(self, group_name):
        try:
            group = Group.objects.get(name=group_name)
            return group in self.groups.all()
        except Group.DoesNotExist:
            return False

    def belongs_to_groups(self, group_names):
        try:
            groups = list(Group.objects.filter(name__in=group_names))
            return any(group in self.groups.all() for group in groups)
        except Exception as e:
            return False

    def get_language(self):
        try:
            iso_code = self.language.first().iso_code
            return iso_code
        except Exception:
            return "en"

    def get_nextcloud_user_id(self):
        if self.nc_app_password:
            return self.username
        else:
            return None

    class Meta:
        db_table = "auth_user"
        verbose_name = gettext_lazy("users_model_name")
        verbose_name_plural = gettext_lazy("users_model_plural_name")
        constraints = [
            models.UniqueConstraint(
                Lower("username"),
                name="username_unique_value",
                violation_error_message=gettext_lazy("The username already exists"),
            ),
        ]


def get_super_admin():
    try:
        return User.objects.get(email=SUPERADMIN_EMAIL)
    except User.DoesNotExist:
        raise ValidationError("ERRORS_SUPERADMIN_DOES_NOT_EXIST")


class UserInvitation(User):
    class Meta:
        proxy = True
        verbose_name = gettext_lazy("User invitation")
        verbose_name_plural = gettext_lazy("User invitations")


class OrganizationMemberInvitation(User):
    class Meta:
        proxy = True
        verbose_name = gettext_lazy("Organization member invitation")
        verbose_name_plural = gettext_lazy("Organization member invitations")
