import logging

from .models import OrganizationMemberInvitation, User

from django.contrib.auth.models import Group, Permission, PermissionManager

logger = logging.getLogger(name=__name__)

SUPERADMIN_GROUP_NAME = "Superadmin"
STAFF_GROUP_NAME = "Staff"


def create_organization_member_invitation(
    username: str, fullname: str, email: str
) -> OrganizationMemberInvitation:
    return OrganizationMemberInvitation.objects.create(
        username=username,
        full_name=fullname,
        email=email,
    )


def get_user(nextcloud_user_id: str) -> User:
    return User.objects.get(username=nextcloud_user_id)


def configure_groups() -> None:
    permissions: PermissionManager[Permission] = Permission.objects.all()
    _configure_superadmin_group(permissions=permissions)
    _configure_staff_group(permissions=permissions)


def _configure_superadmin_group(permissions) -> None:
    logger.info(msg="Setting up Superadmin group permissions...")
    superadmin_group: Group = Group.objects.get(name=SUPERADMIN_GROUP_NAME)
    _assign_permissions(permissions=permissions, group=superadmin_group)
    logger.info(msg="Finished setting up Superadmin group permissions.")


def _configure_staff_group(permissions):
    codenames: list[str] = [
        "change_organizationinvitation",
        "view_organizationinvitation",
        "add_organization",
        "change_organization",
        "view_organization",
        "add_userorganizationgroupproxy",
        "change_userorganizationgroupproxy",
        "view_userorganizationgroupproxy",
    ]
    staff_group: Group = Group.objects.get(name=STAFF_GROUP_NAME)
    staff_permissions: PermissionManager[Permission] = permissions.filter(
        codename__in=codenames
    )
    logger.info(
        msg="Setting up Staff group permissions",
        extra={"permissions": staff_permissions},
    )
    _assign_permissions(permissions=staff_permissions, group=staff_group)
    logger.info(msg="Finished setting up Staff group permissions.")


def _assign_permissions(permissions, group) -> None:
    for permission in permissions:
        group.permissions.add(permission)
    group.save()
