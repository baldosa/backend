"""
Conveniece module for accounts forms.
"""

from django import forms
from django.contrib.auth import password_validation
from django.core.exceptions import ValidationError
from django.contrib.auth.forms import (
    PasswordResetForm as _PasswordResetForm,
)
from django.contrib.auth.models import Group
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.sites.models import Site
from django.utils.translation import gettext_lazy

from apps.accounts.models import User, UserInvitation, Language
from apps.sites.common import build_site_with_token
from apps.mails.client import MailingClient
from apps.mails.settings import get_noreply_from
from apps.nextcloud.nextcloud import initialise_user

from colmena.validators.validators import (
    validate_characters,
    validate_password_characters,
)
from colmena.settings.base import BACKEND_PASSWORD_RESET_SITE_NAME


ADMIN_GROUPS = ["Staff", "Superadmin"]


class UserInvitationForm(forms.ModelForm):
    """
    The UserInvitationForm wraps the form that invites Superadmin and Staff
    users.
    """

    def __init__(self, *args, **kwargs):
        super(UserInvitationForm, self).__init__(*args, **kwargs)
        # TODO: Make UserInvitation supports groups as a one to many relationship
        self.fields["assigned_group"].initial = self.instance.groups.first().name
        if self.instance.region:
            self.fields["region"].initial = self.instance.region.name

    error_messages = {
        "password_mismatch": gettext_lazy("The two password fields didn’t match."),
        "username_spaces": gettext_lazy("The username should not contain spaces."),
        "invalid_password_character": gettext_lazy(
            "Password cointains invalid characters"
        ),
        "invalid_characters": gettext_lazy(
            "This field can only contain letters, numbers, and the following characters: .@-_'"
        ),
    }
    new_password = forms.CharField(
        label=gettext_lazy("New password"),
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
        strip=False,
        help_text=password_validation.password_validators_help_text_html(),
    )
    new_password_confirmation = forms.CharField(
        label=gettext_lazy("New password confirmation"),
        strip=False,
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
    )

    assigned_group = forms.CharField(
        label=gettext_lazy("Account Type"),
        disabled=True,
    )

    region = forms.CharField(
        label=gettext_lazy("Region"), disabled=True, required=False
    )

    class Meta:
        """
        Meta class
        """

        model = UserInvitation
        fields = ["full_name", "username", "email", "language"]

    fieldsets = (
        (
            None,
            {
                "fields": (
                    "full_name",
                    "username",
                    "email",
                    "language",
                    "new_password",
                    "new_password_confirmation",
                ),
            },
        ),
        (
            None,
            {
                "fields": ("assigned_group", "region"),
            },
        ),
    )

    def clean_username(self):
        username = self.cleaned_data.get("username")
        if validate_characters(username):
            raise ValidationError(
                self.error_messages["invalid_characters"],
                code="invalid_characters",
            )

        if " " in username:
            raise ValidationError(
                self.error_messages["username_spaces"],
                code="username_spaces",
            )
        return username

    def clean_new_password_confirmation(self):
        new_password = self.cleaned_data.get("new_password")
        new_password_confirmation = self.cleaned_data.get("new_password_confirmation")
        if not (
            validate_password_characters(new_password)
            and validate_password_characters(new_password_confirmation)
        ):
            raise ValidationError(
                self.error_messages["invalid_password_character"],
                code="invalid_password_character",
            )

        if (
            new_password
            and new_password_confirmation
            and new_password != new_password_confirmation
        ):
            raise ValidationError(
                self.error_messages["password_mismatch"],
                code="password_mismatch",
            )
        password_validation.validate_password(new_password_confirmation, self.instance)
        return new_password_confirmation

    def save(self, commit=True):
        user = super().save(commit=False)
        password = self.cleaned_data["new_password"]
        language = self.cleaned_data["language"].first()
        user.set_password(password)
        # FIXME: Add a function in UserInvitation model
        if not user.is_active:
            _user = initialise_user(user.username, password, user)
            _user.is_active = True
            _user.is_staff = True
            _user.language.clear()
            _user.language.add(language.pk)
            _user.save()

            return user


class UserInvitationCreatedForm(forms.ModelForm):
    groups = forms.ModelChoiceField(
        queryset=Group.objects.filter(name__in=["Superadmin", "Staff"]), required=True
    )
    language = forms.ModelChoiceField(queryset=Language.objects.all(), required=True)

    error_messages = {
        "username_spaces": gettext_lazy("The username should not contain spaces."),
    }

    class Meta:
        model = UserInvitation
        fields = ["groups", "username", "full_name", "email", "language", "region"]

    def __init__(self, *args, **kwargs):
        super(UserInvitationCreatedForm, self).__init__(*args, **kwargs)
        self.fields["region"].required = False  # Make region optional

    def clean_username(self):
        username = self.cleaned_data.get("username")
        if " " in username:
            raise ValidationError(
                self.error_messages["username_spaces"],
                code="username_spaces",
            )
        return username

    def clean_groups(self):
        group = self.cleaned_data["groups"]
        return Group.objects.filter(id=group.id)

    def clean_language(self):
        language = self.cleaned_data["language"]
        return Language.objects.filter(id=language.id)

    def get_first_item(self, field):
        return self.cleaned_data.get(field).first()

    def save(self, commit=True):
        user = super(UserInvitationCreatedForm, self).save(commit=False)

        user._invited_by = self.user
        user._group = self.get_first_item("groups")
        user._language = self.get_first_item("language")
        user.save()

        return user

    def save_m2m(self):
        return self._save_m2m()


class PasswordResetForm(_PasswordResetForm):
    email = forms.EmailField(
        label=("Email"),
        max_length=254,
        widget=forms.EmailInput(attrs={"autocomplete": "email"}),
    )

    def save(self):
        # forces fields validation not triggered since we don't have a model associated to the form
        self.full_clean()
        email_address = self.cleaned_data.get("email")
        try:
            user = User.objects.get(email=email_address)
            if user.belongs_to_groups(ADMIN_GROUPS):
                email_language = user.get_language()
                token = PasswordResetTokenGenerator().make_token(user)
                url_path = Site.objects.get(
                    name=BACKEND_PASSWORD_RESET_SITE_NAME
                ).domain
                restore_password_link = build_site_with_token(
                    url_path, {"pk": user.pk, "token": token}
                )
                self.send_mail(
                    user, email_address, email_language, restore_password_link
                )

        except User.DoesNotExist as e:
            pass

    def send_mail(
        self,
        user,
        email_address,
        user_language,
        restore_password_link,
    ):
        mail_client = MailingClient()
        email_template = mail_client.get_template_by_language(
            "user-password-reset", user_language
        )
        context = {
            "user": user,
            "support_team": "Colmena Team",
            "recovery_link": restore_password_link,
        }
        email = mail_client.build(
            get_noreply_from(),
            [email_address],
            email_template,
            context,
        )
        email.send()
