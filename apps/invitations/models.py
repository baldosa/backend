from apps.accounts.models import User, Language
from apps.organizations.models import Organization

from apps.invitations.exceptions import (
    InvalidInvitationGroupException,
    InvalidInvitationTokenException,
)

from colmena.settings.base import FRONTEND_SIGNUP_SITE_NAME, BACKEND_SITE_NAME
from colmena.utils import encoding
from colmena.validators.validators import ValidatorOnlyLettersAndSpaces

from django.contrib.auth.models import Group
from django.contrib.sites.models import Site
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy
from django.urls import reverse

from urllib.parse import urlencode

from .interfaces import SendableModel

from colmena.settings.base import (
    FRONTEND_INVITATION_SITE_NAME,
    FRONTEND_PASSWORD_RESET_SITE_NAME,
)


class InvitationStatus(models.TextChoices):
    PENDING = "PENDING", gettext_lazy("Pending")
    SENT = "SENT", gettext_lazy("Sent")
    ACCEPTED = "ACCEPTED", gettext_lazy("Accepted")
    DELIVERY_FAILED = "DELIVERY_FAILED", gettext_lazy("Delivery failed")


# Create your models here
class Invitation(SendableModel):
    """
    Abstract class to implement specific invitation classes.
    """

    invited_by = models.ForeignKey(
        User,
        verbose_name=gettext_lazy("invitations_prop_invited_by"),
        on_delete=models.SET_NULL,
        related_name="%(class)s_from_user",
        null=True,
    )
    user = models.ForeignKey(
        User,
        verbose_name=gettext_lazy("invitations_prop_user"),
        on_delete=models.CASCADE,
        related_name="%(class)s_user",
        null=False,
    )
    token = models.CharField(
        gettext_lazy("invitations_prop_token"),
        max_length=128,
        validators=[ValidatorOnlyLettersAndSpaces()],
    )

    state = models.CharField(
        gettext_lazy("invitations_prop_state"),
        max_length=32,
        choices=InvitationStatus.choices,
        default=InvitationStatus.PENDING,
    )
    created_at = models.DateTimeField(
        gettext_lazy("created_at"), default=timezone.now, editable=False
    )
    updated_at = models.DateTimeField(
        gettext_lazy("updated_at"), default=timezone.now, editable=True
    )

    def save(self, *args, **kwargs) -> None:
        self.updated_at = timezone.now()
        super(Invitation, self).save(*args, **kwargs)

    def mark_as_sent(self) -> None:
        self.state = InvitationStatus.SENT
        self.save()

    def mark_as_accepted(self) -> None:
        self.state = InvitationStatus.ACCEPTED
        self.save()

    def mark_as_delivery_failed(self) -> None:
        self.state = InvitationStatus.DELIVERY_FAILED
        self.save()

    def can_resend(self):
        return self.state != InvitationStatus.ACCEPTED

    def refresh_token(self, token: str):
        self.token = token
        self.save()

    def _check_token_by_user(self, token: str, user: User) -> bool:
        return PasswordResetTokenGenerator().check_token(user, token)

    class Meta:
        abstract = True


class AdministrationInvitation(Invitation):
    """
    Specific `Invitation` class to manage administration invitations.
    """

    group = models.ForeignKey(
        Group,
        verbose_name=gettext_lazy("invitations_prop_group"),
        on_delete=models.DO_NOTHING,
        null=True,
        limit_choices_to={"name__in": ["Superadmin", "Staff"]},
    )

    language = models.ForeignKey(
        Language,
        verbose_name=gettext_lazy("languages_model_name"),
        on_delete=models.DO_NOTHING,
        null=False,
        default=3,
    )

    def get_template_name(self) -> str:
        if self.can_resend():
            return "staff-invitation"
        return "staff-confirmation"

    def get_invitation_link(self) -> str:
        if self.can_resend():
            encoded_id = encoding.base64_encode_int(self.id)
            return f'{self.domain}{reverse("invitations_register",args=[encoded_id])}'
        return self.domain

    def get_recovery_link(self) -> str:
        return f"{self.domain}/password/reset/"

    def get_language(self) -> Language:
        return self.language

    def get_email_context(self) -> dict:
        base = {
            "user": self.user,
            "role": self.group.name,
        }
        region = self.user.region
        if self.user.region:
            base.update({"region": region.name})
        if self.can_resend():
            extra_context = {
                "invitation_link": self.get_invitation_link(),
            }
        else:
            extra_context = {
                "confirmation_link": self.get_invitation_link(),
                "recovery_link": self.get_recovery_link(),
            }
        base.update(extra_context)
        return base

    @property
    def domain(self) -> str:
        return Site.objects.get(name=BACKEND_SITE_NAME).domain

    def validate(self) -> "AdministrationInvitation":
        token: str = self.token
        user: User = self.user

        if not user.belongs_to_groups(["Superadmin", "Staff"]):
            raise InvalidInvitationGroupException
        if not self._check_token_by_user(token, user):
            raise InvalidInvitationTokenException

        return self

    def __str__(self) -> str:
        return f"Invitation id={str(self.id)}"

    class Meta:
        verbose_name: str = gettext_lazy("administration_invitation_model_name")
        verbose_name_plural: str = gettext_lazy(
            "administration_invitation_plural_model_name"
        )


class OrganizationInvitation(Invitation):
    """
    Specific `Invitation` class to manage organization invitations.
    """

    organization = models.ForeignKey(
        Organization,
        verbose_name=gettext_lazy("invitations_prop_organization"),
        on_delete=models.SET_NULL,
        null=True,
    )

    group = models.ForeignKey(
        Group,
        verbose_name=gettext_lazy("invitations_prop_group"),
        on_delete=models.DO_NOTHING,
        null=True,
        limit_choices_to={"name__in": ["OrgOwner", "User", "Admin"]},
    )

    def get_template_name(self) -> str:
        if self.can_resend():
            return "member-organization-invitation"
        return "user-confirmation"

    def get_invitation_link(self) -> str:
        site_domain: str = Site.objects.get(name=FRONTEND_SIGNUP_SITE_NAME).domain
        encoded_invitation_id: str = encoding.base64_encode_int(self.id)
        url: str = site_domain.replace(":invitation_id", str(encoded_invitation_id))
        query_params: str = urlencode(query={"username": self.user.username})
        return f"{url}?{query_params}"

    def get_email_context(self) -> dict:
        base = {
            "user": self.user,
            "organization": self.organization,
            "support_web": "https://colmena.dw.com/support",
            "support_team": "Colmena Team",
        }
        if self.can_resend():
            extra_context = {
                "invitation_link": self.get_invitation_link(),
            }
        else:
            extra_context = {
                "login_url": Site.objects.get(
                    name=FRONTEND_INVITATION_SITE_NAME
                ).domain,
                "recovery_link": Site.objects.get(
                    name=FRONTEND_PASSWORD_RESET_SITE_NAME
                )
                .domain.replace("<pk>-", "")
                .replace("<token>", ""),
            }
        base.update(extra_context)
        return base

    def get_language(self) -> Language:
        return self.organization.language.iso_code

    def validate(self) -> "OrganizationInvitation":
        token: str = self.token
        user: User = self.user

        if not user.belongs_to_groups(["OrgOwner", "Admin", "User"]):
            raise InvalidInvitationGroupException
        if not self._check_token_by_user(token, user):
            raise InvalidInvitationTokenException

        return self

    def __str__(self) -> str:
        return f"Invitation id={str(self.id)}"

    class Meta:
        verbose_name: str = gettext_lazy("organization_invitation_model_name")
        verbose_name_plural: str = gettext_lazy(
            "organization_invitation_plural_model_name"
        )
