from rest_framework import serializers

from apps.invitations import invitations
from apps.invitations.exceptions import (
    UnauthorizedUserException,
)
from apps.invitations.models import OrganizationInvitation, InvitationStatus
from apps.organizations.serializers import (
    OrganizationMemberSerializer,
    OrganizationMemberGroupSerializer,
)


class ListInvitationsResponseSerializer(serializers.ModelSerializer):
    group = OrganizationMemberGroupSerializer()
    user = OrganizationMemberSerializer()
    invited_by = OrganizationMemberSerializer()

    class Meta:
        model = OrganizationInvitation
        fields = ("invited_by", "user", "group", "state", "id")


class ListInvitationsRequestSerializer(serializers.Serializer):
    organization_id = serializers.IntegerField()
    state = serializers.ChoiceField(choices=InvitationStatus.choices, required=False)

    def to_internal_value(self, data):
        if "state" in data:
            data["state"] = data["state"].upper()
        return super().to_internal_value(data)

    def is_valid(self, *, raise_exception=True):
        super().is_valid(raise_exception=True)
        if not invitations.can_list_invitation(
            self.context.get("user"), self.initial_data.get("organization_id")
        ):
            raise UnauthorizedUserException


class ResendInvitationRequestSerializer(serializers.Serializer):
    invitation_id = serializers.IntegerField(required=True)


class ResendInvitationResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrganizationInvitation
        exclude = ["token"]
