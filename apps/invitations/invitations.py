from .exceptions import (
    InvitationNotFoundException,
    InvitationAlreadyAcceptedException,
    UnauthorizedUserException,
)
from .models import OrganizationInvitation, AdministrationInvitation, Invitation

from apps.accounts.models import User
from apps.mails import invitations as email_invitations
from apps.nextcloud import nextcloud
from apps.organizations import organizations
from apps.organizations.models import Organization

from colmena.serializers.serializers import ErrorSerializer
from colmena.utils import encoding

from django.contrib.auth.models import Group
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.core.exceptions import ValidationError

from ..organizations.organizations import user_has_any_role_in_organization
from ..accounts.models import Language
from ..accounts.common import is_superadmin


def new_organization_members(
    members_data, organization: Organization, invited_by: User
) -> None:
    for member_data in members_data:
        (
            user,
            organization,
            group,
        ) = organizations.create_organization_member(
            username=_build_username_from_email(email=member_data["email"]),
            fullname=member_data["full_name"],
            email=member_data["email"],
            organization=organization,
            group_id=member_data["group_id"],
        )
        create_organization_invitation(
            user=user,
            invited_by=invited_by,
            organization=organization,
            group=group,
        )


def create_organization_invitation(
    user: User, invited_by: User, organization: Organization, group: Group
) -> OrganizationInvitation:
    token: str = generate_token_by_user(user)

    return OrganizationInvitation.objects.create(
        invited_by=invited_by,
        user=user,
        organization=organization,
        token=token,
        group=group,
    )


"""
Given a user, an `invited_by` user a group and a language, returns a new invitation for Administration usage.
"""


def create_administration_invitation(
    user: User, invited_by: User, group: Group, language: Language
) -> AdministrationInvitation:
    token = generate_token_by_user(user)
    return AdministrationInvitation.objects.create(
        user=user, invited_by=invited_by, group=group, token=token, language=language
    )


def confirm(
    invitation: OrganizationInvitation, username: str, password: str
) -> OrganizationInvitation:
    # Initialise nextcloud user
    user: User = nextcloud.initialise_user(username, password, invitation.user)
    invitation.user = user
    invitation.user.save()
    # Use new_username because the user was don't save yet.
    nextcloud_user_id: str = username
    # organization_owner = invitation.organization.get_organization_owner_user()
    # Initialise talk configuration
    nextcloud.initialise_talk(invitation.user, nextcloud_user_id)
    # Initialise project configuration
    nextcloud.initialise_projects(invitation.user, nextcloud_user_id)
    # Initialise groups and workspaces
    organizations.initialise_groups(
        user=invitation.user,
        organization=invitation.organization,
        group=invitation.group,
        nextcloud_user_id=nextcloud_user_id,
        invited_by=invitation.invited_by,
    )

    invitation.user.is_active = True
    invitation.user.save()
    invitation.mark_as_accepted()

    email_invitations.send_invitation(invitation=invitation)

    return invitation


def generate_token_by_user(user: User) -> str:
    return PasswordResetTokenGenerator().make_token(user=user)


def validate_invitation(encoded_invitation_id: str, invitation_type: str) -> Invitation:
    invitation: Invitation = _get_invitation_by_encoded_id(
        encoded_invitation_id, invitation_type
    )
    return invitation.validate()


def _get_invitation_by_encoded_id(
    encoded_invitation_id: str, invitation_type: str
) -> OrganizationInvitation:
    try:
        invitation_id: int = encoding.base64_decode_int(encoded_invitation_id)
        if invitation_type == "organization":
            return OrganizationInvitation.objects.get(pk=invitation_id)
        elif invitation_type == "administration":
            return AdministrationInvitation.objects.get(pk=invitation_id)
        else:
            raise Exception
    except Exception:
        raise ValidationError(ErrorSerializer("ERRORS_INVALID_REQUEST").data)


def _build_username_from_email(email: str) -> str:
    return email.split("@")[0]


def list_invitations(params: dict = {}):
    return OrganizationInvitation.objects.filter(**params)


def get_invitation_by_id(invitation_id: int) -> OrganizationInvitation:
    """
    Given an id, returns an `Invitation` object.
    """
    try:
        return OrganizationInvitation.objects.get(pk=invitation_id)
    except Exception:
        raise InvitationNotFoundException


def send_invitation(invitation_id: int, user: User) -> OrganizationInvitation:
    """
    Given an `invitation_id` and a `user`, updates an invitation and sends a notification.
    """
    invitation: OrganizationInvitation = get_invitation_by_id(invitation_id)
    if not _can_send_invitation(user, invitation.organization.id):
        raise UnauthorizedUserException
    if not invitation.can_resend():
        raise InvitationAlreadyAcceptedException
    invitation.refresh_token(generate_token_by_user(invitation.user))
    email_invitations.send_invitation(invitation=invitation)
    invitation.mark_as_sent()
    return invitation


def can_list_invitation(user: User, organization_id):
    return user_has_any_role_in_organization(
        user, ["Admin", "OrgOwner"], organization_id
    )


def _can_send_invitation(user, organization_id):
    return (
        is_superadmin(user)
        or _is_created_by(user, organization_id)
        or user_has_any_role_in_organization(
            user, ["Admin", "OrgOwner"], organization_id
        )
    )


def _is_created_by(user: User, organization_id: int) -> bool:
    organization = organizations.get_organization(organization_id)
    return organization.is_created_by(user.id)


def get_administration_invitation(params: dict) -> AdministrationInvitation:
    return AdministrationInvitation.objects.get(**params)
