from . import invitations
from .models import OrganizationInvitation

from apps.accounts.models import Language, User, get_super_admin
from apps.accounts.support import user_fixture, language_fixture
from apps.invitations.support import invitation_fixture
from apps.nextcloud.resources import groups as nextcloud_group_manager
from apps.nextcloud.resources import talk as nextcloud_talk_manager
from apps.nextcloud.resources import users as nextcloud_user_manager
from apps.nextcloud.resources import utils as nextcloud_utils
from apps.organizations.models import (
    Organization,
    Team,
    UserOrganizationGroupProxy,
    UserTeam,
)
from apps.organizations.organizations import get_personal_workspace
from apps.organizations.resources import team as team_manager
from apps.organizations.support import organization_fixture

from colmena.utils import encoding

from django.contrib.auth.models import Group
from django.core.exceptions import BadRequest
from django.urls import reverse
from django.test import TestCase, tag

from rest_framework import status
from rest_framework.test import APITestCase


class TestInvitationModel(TestCase):
    fixtures = [
        "apps/sites/seeds/00-sites.json",
        "apps/accounts/seeds/02-groups.json",
        "apps/accounts/seeds/04-languages.json",
    ]

    def setUp(self):
        self.user = user_fixture.create()
        self.invited_by = user_fixture.create()
        language = Language.objects.get(iso_code="en")
        self.organization = organization_fixture.create({"language": language})

    def test_create_an_org_owner_invitation_with_valid_attrs_returns_a_new_organization(
        self,
    ):
        org_owner_group: Group = Group.objects.get(name="OrgOwner")
        organization_invitation: OrganizationInvitation = (
            invitations.create_organization_invitation(
                self.user, self.invited_by, self.organization, org_owner_group
            )
        )

        self.assertIsNotNone(organization_invitation.token)

    def test_create_an_org_admin_invitation_with_valid_attrs_returns_a_new_organization(
        self,
    ):
        org_admin_group: Group = Group.objects.get(name="Admin")
        organization_invitation: OrganizationInvitation = (
            invitations.create_organization_invitation(
                self.user, self.invited_by, self.organization, org_admin_group
            )
        )

        self.assertIsNotNone(organization_invitation.token)


class TestConfirmInvitationView(APITestCase):
    fixtures = [
        "apps/sites/seeds/00-sites.json",
        "apps/accounts/seeds/02-groups.json",
        "apps/accounts/seeds/04-languages.json",
    ]

    def setUp(self):
        # Languages
        self.language: Language = language_fixture.create()

        # Groups
        self.superadmin_group: Group = Group.objects.get(name="Superadmin")
        self.staff_group: Group = Group.objects.get(name="Staff")
        self.org_owner_group: Group = Group.objects.get(name="OrgOwner")
        self.user_group: Group = Group.objects.get(name="User")
        self.admin_group: Group = Group.objects.get(name="Admin")

        # Users
        self.user: User = user_fixture.create({"is_superuser": False})
        self.user.groups.add(self.org_owner_group)
        self.user.language.add(self.language)

        self.admin_user = user_fixture.create(
            {
                "is_superuser": False,
                "is_staff": False,
            }
        )
        self.admin_user.language.add(self.language)

        # Passwords
        self.valid_password = "Ad_12da12:?1d"
        self.short_password = "12?ADSasd"
        self.only_alpha_and_digit_password = "Passwordpassword1340"
        self.only_lowercase_and_digit_password = "pass12345__wordpassword"
        self.only_digit_password = "1234567892342983254328"

        # Organizations
        self.organization: Organization = organization_fixture.create(
            {"language": self.language}
        )

        # UserOrganizationGroups
        UserOrganizationGroupProxy.objects.create(
            user=self.user,
            organization=self.organization,
            group=self.user_group,
        )
        UserOrganizationGroupProxy.objects.create(
            user=self.admin_user,
            organization=self.organization,
            group=self.org_owner_group,
        )

        # Static vars
        self.nc_password_length = 72

    def test_invitation_fails_on_http_methods_other_than_post(self):
        # Setup
        invitation: OrganizationInvitation = invitation_fixture.create(
            {
                "user": self.user,
                "invited_by": self.admin_user,
                "organization": self.organization,
                "group": self.org_owner_group,
            }
        )

        url: str = reverse(
            "confirm_organization_invitation",
            args=[encoding.base64_encode_int(invitation.pk)],
        )
        data = {"username": self.user.username, "password": self.valid_password}

        # Exercise + Verify
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        response = self.client.put(url, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        response = self.client.patch(url, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        response = self.client.delete(url, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    @tag("nextcloud")
    def test_an_invitation_is_sent_when_the_token_is_valid(self):
        # Setup
        group_name: str = nextcloud_group_manager.build_group_name(
            org_id=self.organization.id, team_identifier="general"
        )
        user_fixture.create_superadmin()
        self.superadmin = get_super_admin()

        invitation: OrganizationInvitation = invitation_fixture.create(
            {
                "user": self.user,
                "invited_by": self.superadmin,
                "organization": self.organization,
                "group": self.org_owner_group,
            }
        )

        url: str = reverse(
            "confirm_organization_invitation",
            args=[encoding.base64_encode_int(invitation.pk)],
        )
        data = {"username": self.user.username, "password": self.valid_password}

        # Exercise
        response = self.client.post(url, data=data, format="json")

        # Verify
        self.assertNotEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Teardown
        nextcloud_group_manager.remove_group(
            group_id=group_name,
            user=self.superadmin,
        )
        nextcloud_user_manager.delete_user(
            user_id=self.user.username,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )
        with self.assertRaises(BadRequest) as context:
            nextcloud_user_manager.get_user(
                user_id=self.user.username,
                auth_user_id=self.superadmin.get_nextcloud_user_id(),
                auth_user_app_password=self.superadmin.nc_app_password,
            )

    @tag("nextcloud")
    def test_an_invitation_fails_when_the_nextcloud_admin_does_not_exist(self):
        # Setup
        user_fixture.create_superadmin()
        self.superadmin = get_super_admin()

        # Verify
        self.assertEqual(self.user.nc_app_password, "")

        with self.assertRaises(BadRequest):
            nextcloud_user_manager.get_user(
                user_id=self.user.username,
                auth_user_id=self.superadmin.get_nextcloud_user_id(),
                auth_user_app_password=self.superadmin.nc_app_password,
            )

    @tag("nextcloud")
    def test_a_nextcloud_user_is_created_on_successful_invitation(self):
        # Setup
        group_name: str = nextcloud_group_manager.build_group_name(
            org_id=self.organization.id, team_identifier="general"
        )
        user_fixture.create_superadmin()
        self.superadmin: User = get_super_admin()

        invitation: OrganizationInvitation = invitation_fixture.create(
            {
                "user": self.user,
                "invited_by": self.superadmin,
                "organization": self.organization,
                "group": self.org_owner_group,
            }
        )

        url: str = reverse(
            "confirm_organization_invitation",
            args=[encoding.base64_encode_int(invitation.pk)],
        )
        data = {"username": self.user.username, "password": self.valid_password}

        # Verify
        self.assertEqual(self.user.nc_app_password, "")
        with self.assertRaises(BadRequest):
            nextcloud_user_manager.get_user(
                user_id=self.user.username,
                auth_user_id=self.superadmin.get_nextcloud_user_id(),
                auth_user_app_password=self.superadmin.nc_app_password,
            )

        # Exercise
        self.client.post(url, data=data, format="json")

        # Verify
        updated_user: User = User.objects.get(username=self.user.username)
        nc_user = nextcloud_user_manager.get_user(
            user_id=updated_user,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )
        nc_group_name = nextcloud_group_manager.get_group(
            group_id=group_name, user=updated_user
        )
        nc_conversation = nextcloud_talk_manager.search_user_conversation(
            filter_value=group_name,
            user=updated_user,
        )
        # Test standard length from app_password created with cli
        self.assertEqual(len(updated_user.nc_app_password), self.nc_password_length)

        self.assertEqual(nc_user["id"], updated_user.username)
        self.assertTrue(nc_user["enabled"])
        self.assertEqual(nc_user["email"], updated_user.email)
        self.assertEqual(nc_user["displayname"], updated_user.full_name)
        self.assertEqual(nc_group_name, group_name)
        self.assertEqual(nc_conversation["name"], group_name)

        # Teardown
        nextcloud_group_manager.remove_group(
            group_id=group_name,
            user=self.superadmin,
        )
        nextcloud_user_manager.delete_user(
            user_id=self.user.username,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )
        with self.assertRaises(BadRequest) as context:
            nextcloud_user_manager.get_user(
                user_id=self.user.username,
                auth_user_id=self.superadmin.get_nextcloud_user_id(),
                auth_user_app_password=self.superadmin.nc_app_password,
            )

    @tag("skip")
    def test_a_nextcloud_user_is_created_on_successful_invitation_for_an_organization_admin(
        self,
    ):
        # General Setup
        organization: Organization = organization_fixture.create()
        group_name: str = nextcloud_group_manager.build_group_name(
            org_id=organization.id, team_identifier="general"
        )
        user_fixture.create_superadmin()
        superadmin: User = get_super_admin()

        # Setup OrgOwner user
        UserOrganizationGroupProxy.objects.filter(
            user=self.user,
            organization=organization,
            group=self.user_group,
        ).update(group=self.org_owner_group)

        nextcloud_user_manager.create_user(
            user_id=self.user.username,
            display_name=self.user.full_name,
            user_email=self.user.email,
            password=self.valid_password,
            auth_user_id=superadmin.get_nextcloud_user_id(),
            auth_user_app_password=superadmin.nc_app_password,
        )
        nc_app_password: str | None = nextcloud_user_manager.create_app_password(
            self.user.username, self.valid_password
        )
        self.user.nc_app_password = nc_app_password
        self.user.save()
        # Create Group
        nextcloud_group_manager.create_group(group_id=group_name, user=superadmin)
        # Add user in group
        nextcloud_group_manager.add_user_to_group(
            group_id=group_name,
            user_id=self.user.username,
            user=superadmin,
        )
        # Provides conversation creation privileges to the Admin
        nextcloud_group_manager.promote_user_to_subadmin(
            group_id=group_name,
            user_id=self.user.username,
            user=superadmin,
        )
        # Create a conversation group
        nc_conversation = nextcloud_talk_manager.create_group_conversation(
            user_id=self.user.get_nextcloud_user_id(),
            room_name=group_name,
            group_id=group_name,
            user=self.user,
        )

        # Setup Admin user
        admin_user: User = user_fixture.create(attrs={"is_superuser": False})
        admin_user.groups.add(self.admin_group)
        admin_user.language.add(self.language)
        admin_user.save()

        # Create Team
        organization_team: Team = Team.objects.create(
            nc_group_id=group_name,
            nc_conversation_token=nextcloud_utils.get_conversation_token(
                nc_conversation
            ),
            organization=organization,
        )

        # Setup Admin user in organization
        UserOrganizationGroupProxy.objects.create(
            user=admin_user,
            organization=organization,
            group=self.admin_group,
        )
        # Setup post
        invitation: OrganizationInvitation = invitation_fixture.create(
            {
                "user": admin_user,
                "invited_by": self.user,
                "organization": organization,
                "group": self.org_owner_group,
            }
        )

        url: str = reverse(
            "confirm_organization_invitation",
            args=[encoding.base64_encode_int(invitation.pk)],
        )
        data = {"username": admin_user.username, "password": self.valid_password}

        # Verify
        self.assertEqual(admin_user.nc_app_password, "")
        with self.assertRaises(BadRequest):
            nextcloud_user_manager.get_user(
                user_id=admin_user.username,
                auth_user_id=superadmin.get_nextcloud_user_id(),
                auth_user_app_password=superadmin.nc_app_password,
            )

        # Exercise
        self.client.post(url, data=data, format="json")

        invitation: OrganizationInvitation = OrganizationInvitation.objects.get(
            id=invitation.id
        )

        print(
            "invitation.user.get_nextcloud_user_id",
            invitation.user.get_nextcloud_user_id(),
        )

        # Verify
        nc_user = nextcloud_user_manager.get_user(
            user_id=invitation.user.get_nextcloud_user_id(),
            auth_user_id=superadmin.get_nextcloud_user_id(),
            auth_user_app_password=superadmin.nc_app_password,
        )
        nc_group_name = nextcloud_group_manager.get_group(
            group_id=group_name, user=invitation.user
        )
        group_members = nextcloud_group_manager.get_group_members(
            group_id=group_name, user=invitation.user
        )
        nc_conversation = nextcloud_talk_manager.search_user_conversation(
            filter_value=group_name,
            user=invitation.user,
        )
        user_team: UserTeam = UserTeam.objects.get(
            user=invitation.user, team=organization_team
        )
        personal_workspace_team = get_personal_workspace(
            invitation.user.get_nextcloud_user_id()
        )

        # Test standard length from app_password created with cli
        self.assertEqual(len(invitation.user.nc_app_password), self.nc_password_length)

        self.assertEqual(nc_user["id"], invitation.user.username)
        self.assertTrue(nc_user["enabled"])
        self.assertEqual(nc_user["email"], invitation.user.email)
        self.assertEqual(nc_user["displayname"], invitation.user.full_name)
        self.assertEqual(nc_group_name, group_name)
        self.assertEqual(nc_conversation["name"], group_name)
        self.assertEqual(len(group_members), 2)
        self.assertTrue(group_members.__contains__(self.user.username))
        self.assertTrue(group_members.__contains__(invitation.user.username))

        # Test UserTeam
        self.assertIsInstance(user_team, UserTeam)

        # Teardown
        team_manager.remove_from_team(invitation.user, organization_team, superadmin)
        team_manager.delete_personal_workspace(personal_workspace_team, superadmin)
        team_manager.delete(organization_team, superadmin)
        nextcloud_user_manager.delete_user(
            user_id=invitation.user.username,
            auth_user_id=superadmin.get_nextcloud_user_id(),
            auth_user_app_password=superadmin.nc_app_password,
        )
        with self.assertRaises(BadRequest) as context:
            nextcloud_user_manager.get_user(
                user_id=invitation.user.username,
                auth_user_id=superadmin.get_nextcloud_user_id(),
                auth_user_app_password=superadmin.nc_app_password,
            )
        nextcloud_user_manager.delete_user(
            user_id=self.user.username,
            auth_user_id=superadmin.get_nextcloud_user_id(),
            auth_user_app_password=superadmin.nc_app_password,
        )

    def test_fails_when_the_password_is_short(self):
        # Setup
        error_password_is_short = (
            "This password is too short. It must contain at least 12 characters."
        )
        code_password_is_short = "password_too_short"

        invitation: OrganizationInvitation = invitation_fixture.create(
            {
                "user": self.user,
                "invited_by": self.admin_user,
                "group": self.user_group,
                "organization": self.organization,
            }
        )

        url: str = reverse(
            "confirm_organization_invitation",
            args=[encoding.base64_encode_int(invitation.pk)],
        )
        data = {"username": self.user.username, "password": self.short_password}

        # Exercise
        response = self.client.post(url, data=data, format="json")

        # Verify
        error_list = response.data["password"]
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(error_list[0].code, code_password_is_short)
        self.assertEqual(str(error_list[0]), error_password_is_short)

    def test_fails_when_the_password_lacks_a_special_character(self):
        # Setup
        invalid_password = "This password must contain at least 1 special character."
        invalid_password_coded = "min_length_special_characters"

        invitation: OrganizationInvitation = invitation_fixture.create(
            {
                "user": self.user,
                "invited_by": self.admin_user,
                "group": self.user_group,
                "organization": self.organization,
            }
        )

        url: str = reverse(
            "confirm_organization_invitation",
            args=[encoding.base64_encode_int(invitation.pk)],
        )
        data = {
            "username": self.user.username,
            "password": self.only_alpha_and_digit_password,
        }

        # Exercise
        response = self.client.post(url, data=data, format="json")

        # Verify
        error_list = response.data["password"]
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(error_list[0].code, invalid_password_coded)
        self.assertEqual(str(error_list[0]), invalid_password)

    def test_fails_when_the_password_lacks_an_uppercase_letter(self):
        # Setup
        invalid_password = "This password must contain at least 1 upper case letter."
        invalid_password_code = "min_length_upper_characters"

        invitation: OrganizationInvitation = invitation_fixture.create(
            {
                "user": self.user,
                "invited_by": self.admin_user,
                "group": self.user_group,
                "organization": self.organization,
            }
        )

        url: str = reverse(
            "confirm_organization_invitation",
            args=[encoding.base64_encode_int(invitation.pk)],
        )
        data = {
            "username": self.user.username,
            "password": self.only_lowercase_and_digit_password,
        }

        # Exercise
        response = self.client.post(url, data=data, format="json")

        # Verify
        error_list = response.data["password"]
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(error_list[0].code, invalid_password_code)
        self.assertEqual(str(error_list[0]), invalid_password)

    def test_fails_when_the_password_lacks_alphabet_characters(self):
        # Setup
        invalid_password = "This password is entirely numeric."
        invalid_password_code = "password_entirely_numeric"

        invitation: OrganizationInvitation = invitation_fixture.create(
            {
                "user": self.user,
                "invited_by": self.admin_user,
                "group": self.user_group,
                "organization": self.organization,
            }
        )

        url: str = reverse(
            "confirm_organization_invitation",
            args=[encoding.base64_encode_int(invitation.pk)],
        )
        data = {"username": self.user.username, "password": self.only_digit_password}

        # Exercise
        response = self.client.post(url, data=data, format="json")

        # Verify
        error_list = response.data["password"]
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(error_list[0].code, invalid_password_code)
        self.assertEqual(str(error_list[0]), invalid_password)

    def test_fails_when_the_username_is_invalid(self):
        # Setup
        invitation: OrganizationInvitation = invitation_fixture.create(
            {
                "user": self.user,
                "invited_by": self.admin_user,
                "group": self.user_group,
                "organization": self.organization,
            }
        )

        url: str = reverse(
            "confirm_organization_invitation",
            args=[encoding.base64_encode_int(invitation.pk)],
        )
        data = {
            "username": user_fixture.invalid_attrs()["username"],
            "password": self.valid_password,
        }

        # Exercise
        response = self.client.post(url, data=data, format="json")

        # Verify
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_fails_when_the_username_already_exists(self):
        # Setup
        another_user = user_fixture.create()

        invitation: OrganizationInvitation = invitation_fixture.create(
            {
                "user": self.user,
                "invited_by": self.admin_user,
                "group": self.user_group,
                "organization": self.organization,
            }
        )

        url: str = reverse(
            "confirm_organization_invitation",
            args=[encoding.base64_encode_int(invitation.pk)],
        )
        data = {"username": another_user.username, "password": self.valid_password}

        # Exercise
        response = self.client.post(url, data=data, format="json")

        # Verify
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_fails_when_the_user_belongs_to_a_superadmin_group(self):
        # Set up superadmin user
        superadmin_user = user_fixture.create({"is_superuser": True})
        superadmin_user.groups.add(self.superadmin_group)

        invitation: OrganizationInvitation = invitation_fixture.create(
            {
                "user": superadmin_user,
                "invited_by": self.admin_user,
                "group": self.superadmin_group,
                "organization": self.organization,
            }
        )

        url: str = reverse(
            "confirm_organization_invitation",
            args=[encoding.base64_encode_int(invitation.pk)],
        )
        data = {"username": self.user.username, "password": self.valid_password}

        # Exercise
        response = self.client.post(url, data=data, format="json")

        # Verify
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    @tag("nextcloud")
    def test_fails_when_the_user_belongs_to_a_staff_group(self):
        # Set up staff user
        staff_user = user_fixture.create({"is_superuser": False})
        staff_user.groups.add(self.staff_group)

        invitation: OrganizationInvitation = invitation_fixture.create(
            {
                "user": staff_user,
                "invited_by": self.admin_user,
                "group": self.staff_group,
                "organization": self.organization,
            }
        )

        url: str = reverse(
            "confirm_organization_invitation",
            args=[encoding.base64_encode_int(invitation.pk)],
        )
        data = {"username": staff_user.username, "password": self.valid_password}

        # Exercise
        response = self.client.post(url, data=data, format="json")

        # Verify
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
