from django.core.exceptions import ValidationError


class InvalidInvitationGroupException(ValidationError):
    status_code = 401
    reason: str = "Invitation error. The user does not belong to a valid group."

    def __init__(self) -> None:
        """Configure exception."""
        super(ValidationError, self).__init__()


class InvalidInvitationTokenException(ValidationError):
    status_code = 401
    reason: str = "Invitation error. The token is not valid or expired."

    def __init__(self) -> None:
        """Configure exception."""
        super(ValidationError, self).__init__()


class UnauthorizedUserException(ValidationError):
    status_code = 401
    reason: str = "Authorization Error"

    def __init__(self) -> None:
        """Configure exception."""
        super(ValidationError, self).__init__()


class InvitationAlreadyAcceptedException(ValidationError):
    status_code = 400
    reason: str = "The invitation cannot be resent as it has already been accepted"

    def __init__(self) -> None:
        """Configure exception."""
        super(ValidationError, self).__init__()


class InvitationNotFoundException(ValidationError):
    status_code = 400
    reason: str = "The invitation you are trying to obtain was not found"

    def __init__(self) -> None:
        """Configure exception."""
        super(ValidationError, self).__init__()
