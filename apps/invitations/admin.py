from django.contrib import admin
from apps.invitations.models import OrganizationInvitation, AdministrationInvitation
from apps.organizations.models import Organization
from django.utils.html import format_html
from apps.invitations.invitations import send_invitation
from django.contrib import messages
from django.utils.translation import gettext_lazy


# Register your models here.
@admin.register(OrganizationInvitation)
class OrganizationInvitationAdmin(admin.ModelAdmin):
    list_display = (
        "user",
        "organization",
        "state",
        "invited_by",
        "created_at",
        "updated_at",
        "resend_invitation",
    )

    def resend_invitation(self, obj: OrganizationInvitation):
        if obj.can_resend():
            text = gettext_lazy("Resend")
            return format_html(
                "<button type='submit' name='resend' value='{}' class='button'>{}</button>",
                obj.id,
                text,
            )
        return format_html("<img src='/static/admin/img/icon-no.svg'>")

    resend_invitation.short_description = gettext_lazy("Resend invitation")

    def changelist_view(self, request, extra_context=None):
        if "resend" in request.POST:
            try:
                invitation_id = int(request.POST.get("resend"))
                send_invitation(invitation_id, request.user)
                messages.success(request, gettext_lazy("The invitation has been sent"))
            except Exception:
                messages.error(
                    request, gettext_lazy("The invitation could not be sent")
                )
        return super().changelist_view(request, extra_context)

    def get_queryset(self, request):
        user = request.user
        queryset = OrganizationInvitation.objects.none()
        organizations = Organization.objects.filter(created_by=request.user)
        if request.user.belongs_to_group("Staff"):
            queryset = (
                super().get_queryset(request).filter(organization__in=organizations)
            )
        elif user.is_superuser or user.belongs_to_group("Superadmin"):
            queryset = super().get_queryset(request)
        return queryset


@admin.register(AdministrationInvitation)
class AdministrationInvitationAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        user = request.user
        queryset = AdministrationInvitation.objects.none()
        if request.user.belongs_to_group("Staff"):
            queryset = super().get_queryset(request).filter(invited_by=user)
        elif user.is_superuser or user.belongs_to_group("Superadmin"):
            queryset = AdministrationInvitation.objects.all()
        return queryset
