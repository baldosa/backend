"""
Convenience module for defining views for the invitations app.
"""

from colmena.serializers.serializers import ErrorSerializer
from colmena.utils import encoding

from apps.accounts.exceptions import InvalidPasswordException
from apps.accounts.models import User

from apps.accounts.serializers import (
    ConfirmInvitationSerializer,
    OrganizationMemberInvitationRequestSerializer,
    UserSerializer,
)

from apps.invitations.models import OrganizationInvitation, InvitationStatus
from apps.invitations import invitations
from apps.nextcloud.exceptions import (
    NextcloudAddUserToGroupException,
    NextcloudGroupCreationException,
    NextcloudPasswordCreationException,
    NextcloudUserNotFound,
)
from apps.organizations import organizations
from apps.organizations.exceptions import (
    OrganizationAddTeamMemberException,
    OrganizationCantInviteMembersException,
    OrganizationPersonalWorkspaceCreationException,
    OrganizationTeamCreationException,
)
from apps.organizations.models import Organization

from .exceptions import (
    InvalidInvitationGroupException,
    InvalidInvitationTokenException,
    UnauthorizedUserException,
    InvitationNotFoundException,
    InvitationAlreadyAcceptedException,
)

from .serializers import (
    ListInvitationsRequestSerializer,
    ListInvitationsResponseSerializer,
    ResendInvitationRequestSerializer,
    ResendInvitationResponseSerializer,
)
from drf_spectacular.utils import extend_schema, OpenApiParameter, OpenApiExample

from rest_framework import views, serializers
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response

from ..mails.exceptions import EmailDeliveryException


class CreateInvitationsView(views.APIView):
    """
    The CreateInvitationsView module defines endpoints to manage invitations
    creation.
    """

    serializer_class = OrganizationMemberInvitationRequestSerializer
    permission_classes = [IsAuthenticated]

    @extend_schema(request=OrganizationMemberInvitationRequestSerializer())
    def post(self, request) -> Response:
        serializer = self.serializer_class(data=request.data)
        invited_by = request.user
        if serializer.is_valid():
            try:
                organization_id: int = serializer.data["organization_id"]
                organization: Organization = organizations.get_organization(
                    organization_id
                )
                organizations.validate_invitation_permission(
                    user=invited_by, organization=organization
                )

                invitations.new_organization_members(
                    members_data=serializer.data["new_users"],
                    organization=organization,
                    invited_by=invited_by,
                )

                return Response("ORGANIZATION_MEMBER_INVITATION_CREATED", status=201)

            except OrganizationCantInviteMembersException:
                return Response(
                    ErrorSerializer("ERRORS_INSUFFICIENT_PERMISSIONS").data, status=401
                )
        else:
            return Response(ErrorSerializer("ERRORS_INVALID_REQUEST").data, status=400)


class ConfirmInvitationView(views.APIView):
    """
    The ConfirmInvitationView defines a convenience entrypoint to handle
    user confirmations for received invitations. This view would support
    accepting or even cancelling invitations.
    """

    authentication_classes = []
    permission_classes = [AllowAny]
    serializer_class = ConfirmInvitationSerializer
    queryset = User.objects.all()

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="invitation_id",
                description="The encoded invitation id",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample("Example1", value=encoding.base64_encode_int(5)),
                ],
            ),
        ],
        operation_id="confirm_invitation",
    )
    def post(self, request, **kwargs):
        """
        Given an invitation id and user data, confirms the invitation to
        initialise an existent user account.
        """
        try:
            organization_invitation: OrganizationInvitation = (
                invitations.validate_invitation(
                    encoded_invitation_id=kwargs["invitation_id"],
                    invitation_type="organization",
                )
            )

            data = request.data
            serializer = self.serializer_class(
                data=data,
                queryset=User.objects.all().exclude(pk=organization_invitation.user.pk),
            )

            if not serializer.is_valid():
                return Response(data=serializer.errors, status=400)

            # Hint: Use the information validated in the serializer
            new_username: str = serializer.validated_data["username"]
            new_password: str = serializer.validated_data["password"]

            invitation: OrganizationInvitation = invitations.confirm(
                invitation=organization_invitation,
                username=new_username,
                password=new_password,
            )

            serializer.update(invitation.user, data)

            return Response(data=UserSerializer(invitation.user).data, status=201)

        except InvalidInvitationGroupException:
            return Response(
                data=ErrorSerializer("ERRORS_INVALID_OPERATION").data, status=401
            )
        except InvalidInvitationTokenException:
            return Response(
                data=ErrorSerializer("ERRORS_INVALID_INVITATION_TOKEN").data, status=400
            )
        except InvalidPasswordException:
            return Response(
                data=ErrorSerializer("ERRORS_NEXTCLOUD_INVALID_PASSWORD").data,
                status=400,
            )

        except NextcloudPasswordCreationException:
            return Response(
                data=ErrorSerializer(
                    "ERRORS_NEXTCLOUD_APP_PASSWORD_CREATION_FAILED"
                ).data,
                status=400,
            )

        except NextcloudUserNotFound:
            return Response(
                data=ErrorSerializer("ERRORS_NEXTCLOUD_USER_NOT_FOUND").data, status=404
            )

        except NextcloudGroupCreationException:
            return Response(
                data=ErrorSerializer("ERRORS_NEXTCLOUD_GROUP_CREATION_ERROR").data,
                status=400,
            )

        except NextcloudAddUserToGroupException:
            return Response(
                data=ErrorSerializer("ERRORS_NEXTCLOUD_ADD_USER_TO_GROUP_FAILED").data,
                status=400,
            )

        except OrganizationTeamCreationException:
            return Response(
                data=ErrorSerializer("ERRORS_NEW_TEAM_CREATION_FAILED").data, status=400
            )

        except OrganizationAddTeamMemberException:
            return Response(
                data=ErrorSerializer("ERRORS_ADD_TEAM_MEMBER_FAILED").data, status=400
            )

        except OrganizationPersonalWorkspaceCreationException:
            return Response(
                data=ErrorSerializer("ERRORS_PERSONAL_WORKSPACE_CREATION_FAILED").data,
                status=400,
            )


class InvitationView(views.APIView):
    """
    The InvitationView defines a set of crud operations for the invitations resource.
    """

    serializer_class = ListInvitationsRequestSerializer

    @extend_schema(
        operation_id="list_invitations",
        request=ListInvitationsRequestSerializer,
        responses={
            200: ListInvitationsResponseSerializer(many=True),
            401: {"error_code": "ERRORS_UNAUTHORIZED_USER"},
        },
        parameters=[
            OpenApiParameter(
                name="organization_id",
                description="The encoded organization id",
                location=OpenApiParameter.QUERY,
                type=int,
                required=True,
            ),
            OpenApiParameter(
                name="state",
                description="Filter by the invitation state",
                location=OpenApiParameter.QUERY,
                type=str,
                enum=InvitationStatus.names,
                required=False,
            ),
        ],
    )
    def get(self, request):
        """
        Returns a list of invitations
        """
        data = request.query_params.dict() | {"user": request.user}
        serializer = self.serializer_class(data=data, context={"user": request.user})

        try:
            serializer.is_valid()
            params: dict = {"organization_id": serializer.data.get("organization_id")}
            if request.query_params.get("state") is not None:
                params.update({"state": serializer.data.get("state")})
            invitations_list = invitations.list_invitations(params)
            return Response(
                data=ListInvitationsResponseSerializer(
                    invitations_list, many=True
                ).data,
                status=200,
            )
        except UnauthorizedUserException:
            return Response(
                data=ErrorSerializer("ERRORS_UNAUTHORIZED_USER").data, status=401
            )


class InvitationStatusView(views.APIView):
    """
    The InvitationStatusView defines a set of crud operations for the invitations status resource.
    """

    permission_classes = [IsAuthenticated]

    @extend_schema(
        operation_id="invitations_status",
        responses=serializers.ListSerializer(child=serializers.CharField()),
    )
    def get(self, request, *args, **kwargs):
        """
        Returns a list of invitation status.
        """
        serializer = serializers.ListSerializer(
            child=serializers.CharField(),
            data=InvitationStatus.names,
        )
        return Response(serializer.initial_data, status=200)


class ResendInvitationView(views.APIView):
    """
    The ResendInvitationView defines a set of raw operations for resending invitations.
    """

    serializer_class = ResendInvitationRequestSerializer
    permission_classes = [IsAuthenticated]

    @extend_schema(
        operation_id="resend_invitation",
        request=ResendInvitationRequestSerializer,
        responses=ResendInvitationResponseSerializer,
        parameters=[
            OpenApiParameter(
                name="invitation_id",
                description="The encoded invitation id",
                location=OpenApiParameter.PATH,
                type=int,
                required=True,
            ),
        ],
    )
    def post(self, request, **kwargs):
        data = {"invitation_id": kwargs.get("invitation_id"), "user": request.user}
        serializer = self.serializer_class(data=data, context={"user": request.user})

        try:
            serializer.is_valid()
            return Response(
                data=ResendInvitationResponseSerializer(
                    invitations.send_invitation(
                        serializer.data.get("invitation_id"), request.user
                    )
                ).data,
                status=200,
            )
        except EmailDeliveryException:
            return Response(
                data=ErrorSerializer("ERRORS_COULD_NOT_DELIVERY_INVITATION").data,
                status=400,
            )
        except UnauthorizedUserException:
            return Response(
                data=ErrorSerializer("ERRORS_UNAUTHORIZED_USER").data, status=401
            )
        except InvitationAlreadyAcceptedException:
            return Response(
                data=ErrorSerializer(
                    "ERRORS_THE_INVITATION_HAS_ALREADY_BEEN_ACCEPTED"
                ).data,
                status=400,
            )
        except InvitationNotFoundException:
            return Response(
                data=ErrorSerializer("ERRORS_INVITATION_NOT_FOUND").data, status=400
            )
