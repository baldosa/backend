import io
from django.test import TestCase, tag
from nextcloud_async import NextCloudAsync
from apps.nextcloud.resources import auth as nextcloud_auth
from apps.nextcloud.resources import users as nextcloud_user_manager
from apps.nextcloud.resources import groups as nextcloud_group_manager
from apps.nextcloud.resources import talk as nextcloud_talk_manager
from apps.nextcloud.resources import utils as nextcloud_utils
from apps.nextcloud.resources import files as nextcloud_files_manager
from apps.nextcloud.resources import shares as nextcloud_shares_manager
from apps.nextcloud.config import get_nextcloud_admin_group_name, get_talk_dir
from apps.accounts.support import user_fixture
from apps.organizations.support import organization_fixture
from apps.accounts.models import Language, get_super_admin
from django.core.exceptions import BadRequest, ValidationError
from apps.accounts.models import (
    User,
)
from colmena.settings.base import (
    SUPERADMIN_EMAIL,
    SUPERADMIN_PASSWORD,
)
from nextcloud_async.api.ocs.talk import ConversationType
from django.core.files.uploadedfile import SimpleUploadedFile
from PIL import Image
from django.core.files.storage import default_storage
from django.conf import settings
from colmena.serializers.serializers import ErrorSerializer


def create_dummy_image(filename):
    # Create a dummy image and return a object
    buffer = io.BytesIO()
    image = Image.new("RGB", (100, 100), color="blue")
    image.save(buffer, format="JPEG")
    image_file = SimpleUploadedFile(filename, buffer.getvalue())
    file_path = default_storage.save(filename, image_file)
    full_file_path = settings.MEDIA_ROOT + file_path

    return {"image": image_file, "path": full_file_path}


class TestAuthResources(TestCase):
    fixtures = [
        "apps/accounts/seeds/02-groups.json",
        "apps/accounts/seeds/04-languages.json",
    ]

    def setUp(self):
        self.user = user_fixture.create()

    @tag("nextcloud")
    def test_a_user_without_app_password_cannot_be_authorised(self):
        # Exercise/Verify
        with self.assertRaises(ValidationError) as context:
            nextcloud_auth.auth(
                self.user.get_nextcloud_user_id(), self.user.nc_app_password
            )

    @tag("nextcloud")
    def test_a_superadmin_successfully_authenticates_in_nextcloud(self):
        # Setup
        user_fixture.create_superadmin()
        superadmin_user = User.objects.get(email=SUPERADMIN_EMAIL)

        # Exercise
        nca = nextcloud_auth.auth(
            superadmin_user.get_nextcloud_user_id(), superadmin_user.nc_app_password
        )

        # Verify
        self.assertIsInstance(nca, NextCloudAsync)
        self.assertEquals(nca.user, superadmin_user.username)

    @tag("nextcloud")
    def test_get_superadmin_raises_when_the_superuser_does_not_exist(self):
        # Exercise
        with self.assertRaises(ValidationError) as context:
            get_super_admin()

    @tag("nextcloud")
    def test_get_superadmin_successfully_returns_a_nextcloud_admin(self):
        # Setup
        user_fixture.create_superadmin()
        # Exercise
        nextcloud_admin = get_super_admin()
        # Verify:
        self.assertEqual(nextcloud_admin.email, SUPERADMIN_EMAIL)
        self.assertEqual(nextcloud_admin.username, "superadmin")
        self.assertTrue(nextcloud_admin.is_staff)
        self.assertTrue(nextcloud_admin.is_superuser)


class TestUsersResources(TestCase):
    fixtures = [
        "apps/accounts/seeds/02-groups.json",
        "apps/accounts/seeds/04-languages.json",
    ]

    def setUp(self):
        user_fixture.create_superadmin()
        self.superadmin = User.objects.get(username="superadmin")
        self.user = user_fixture.create()

    @tag("nextcloud")
    def test_create_user_successfully_creates_a_nextcloud_user(self):
        # Setup
        nextcloud_user_manager.create_user(
            user_id=self.user.username,
            display_name=self.user.full_name,
            user_email=self.user.email,
            password=self.user.password,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )
        # Exercise
        nc_user = nextcloud_user_manager.get_user(
            user_id=self.user.username,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )

        self.assertTrue(nc_user["enabled"])
        self.assertEqual(nc_user["email"], self.user.email)
        self.assertEqual(nc_user["displayname"], self.user.full_name)
        self.assertEqual(nc_user["id"], self.user.username)

        # Teardown
        nextcloud_user_manager.delete_user(
            user_id=self.user.username,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )
        with self.assertRaises(BadRequest) as context:
            nextcloud_user_manager.get_user(
                user_id=self.user.username,
                auth_user_id=self.superadmin.get_nextcloud_user_id(),
                auth_user_app_password=self.superadmin.nc_app_password,
            )

    @tag("nextcloud")
    def test_create_user_fails_when_the_user_id_already_exists(self):
        # Exercise
        with self.assertRaises(BadRequest) as context:
            nextcloud_user_manager.create_user(
                user_id=self.superadmin.username,
                display_name=self.user.full_name,
                user_email=self.user.email,
                password=self.user.password,
                auth_user_id=self.superadmin.get_nextcloud_user_id(),
                auth_user_app_password=self.superadmin.nc_app_password,
            )

    @tag("nextcloud")
    def test_get_user_returns_a_nextcloud_user(self):
        # Exercise
        with self.assertRaises(BadRequest) as context:
            nextcloud_user_manager.get_user(
                user_id=self.user.username,
                auth_user_id=self.superadmin.get_nextcloud_user_id(),
                auth_user_app_password=self.superadmin.nc_app_password,
            )

    @tag("nextcloud")
    def test_delete_user_successfully_removes_a_nextcloud_user(self):
        # Setup
        nextcloud_user_manager.create_user(
            user_id=self.user.username,
            display_name=self.user.full_name,
            user_email=self.user.email,
            password=self.user.password,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )

        # Test
        nc_user = nextcloud_user_manager.get_user(
            user_id=self.user.username,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )

        self.assertTrue(nc_user["enabled"])
        self.assertEqual(nc_user["email"], self.user.email)
        self.assertEqual(nc_user["displayname"], self.user.full_name)
        self.assertEqual(nc_user["id"], self.user.username)

        # Exercise
        nextcloud_user_manager.delete_user(
            user_id=self.user.username,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )

        with self.assertRaises(BadRequest) as context:
            nextcloud_user_manager.get_user(
                user_id=self.user.username,
                auth_user_id=self.superadmin.get_nextcloud_user_id(),
                auth_user_app_password=self.superadmin.nc_app_password,
            )

    @tag("nextcloud")
    def test_delete_user_raises_when_the_user_does_not_exist(self):
        # Exercise
        with self.assertRaises(BadRequest) as context:
            nextcloud_user_manager.delete_user(
                user_id=self.user.username,
                auth_user_id=self.superadmin.get_nextcloud_user_id(),
                auth_user_app_password=self.superadmin.nc_app_password,
            )

    @tag("nextcloud")
    def test_create_app_password_successfully_returns_an_app_password(self):
        # Setup
        nextcloud_user_manager.create_user(
            user_id=self.user.username,
            display_name=self.user.full_name,
            user_email=self.user.email,
            password=self.user.password,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )
        # Exercise
        nc_app_password = nextcloud_user_manager.create_app_password(
            self.user.username, self.user.password
        )
        # Verify
        self.assertNotEqual(nc_app_password, None)
        # Teardown
        nextcloud_user_manager.delete_user(
            user_id=self.user.username,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )
        with self.assertRaises(BadRequest) as context:
            nextcloud_user_manager.get_user(
                user_id=self.user.username,
                auth_user_id=self.superadmin.get_nextcloud_user_id(),
                auth_user_app_password=self.superadmin.nc_app_password,
            )

    @tag("nextcloud")
    def test_create_app_password_raises_when_using_invalid_credentials(self):
        # Setup
        nextcloud_user_manager.create_user(
            user_id=self.user.username,
            display_name=self.user.full_name,
            user_email=self.user.email,
            password=self.user.password,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )
        # Exercise
        nc_app_password = nextcloud_user_manager.create_app_password(
            user_fixture.UPDATE_ATTRS["username"], self.user.password
        )
        # Verify
        self.assertEqual(nc_app_password, None)
        # Teardown
        nextcloud_user_manager.delete_user(
            user_id=self.user.username,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )
        with self.assertRaises(BadRequest) as context:
            nextcloud_user_manager.get_user(
                user_id=self.user.username,
                auth_user_id=self.superadmin.get_nextcloud_user_id(),
                auth_user_app_password=self.superadmin.nc_app_password,
            )


class TestGroupResources(TestCase):
    fixtures = [
        "apps/accounts/seeds/02-groups.json",
        "apps/accounts/seeds/04-languages.json",
    ]

    def setUp(self):
        user_fixture.create_superadmin()
        self.superadmin = User.objects.get(username="superadmin")
        self.user = user_fixture.create()
        self.admin_group_name = get_nextcloud_admin_group_name()
        nextcloud_user_manager.create_user(
            user_id=self.user.username,
            display_name=self.user.full_name,
            user_email=self.user.email,
            password=self.user.password,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )
        nc_app_password = nextcloud_user_manager.create_app_password(
            self.user.username, self.user.password
        )
        self.user.nc_app_password = nc_app_password
        self.user.save()

    def tearDown(self):
        nextcloud_group_manager.remove_user_from_group(
            group_id=self.admin_group_name,
            user_id=self.user.username,
            user=self.superadmin,
        )
        nextcloud_user_manager.delete_user(
            user_id=self.user.username,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )
        with self.assertRaises(BadRequest) as context:
            nextcloud_user_manager.get_user(
                user_id=self.user.username,
                auth_user_id=self.superadmin.get_nextcloud_user_id(),
                auth_user_app_password=self.superadmin.nc_app_password,
            )

    @tag("nextcloud")
    def test_create_group_fails_when_the_group_name_is_invalid(self):
        # Exercise
        with self.assertRaises(ValidationError) as context:
            nextcloud_group_manager.create_group(
                group_id=self.admin_group_name, user=self.user
            )

    @tag("nextcloud")
    def test_remove_group_fails_when_the_group_name_is_invalid(self):
        with self.assertRaises(ValidationError) as context:
            nextcloud_group_manager.remove_group(
                group_id=self.admin_group_name, user=self.user
            )

    @tag("nextcloud")
    def test_a_nextcloud_admin_successfully_creates_a_group(self):
        # Setup
        group_name = "test_group"

        # Exercise
        nextcloud_group_manager.create_group(group_id=group_name, user=self.superadmin)

        # Verify
        result = nextcloud_group_manager.get_group(
            group_id=group_name, user=self.superadmin
        )
        self.assertEqual(group_name, result)
        # Teardown
        nextcloud_group_manager.remove_group(group_id=group_name, user=self.superadmin)

    @tag("nextcloud")
    def test_a_nextcloud_admin_successfully_removes_a_group(self):
        # Setup
        group_name = "test_group"
        nextcloud_group_manager.create_group(group_id=group_name, user=self.superadmin)

        # Exercise
        nextcloud_group_manager.remove_group(group_id=group_name, user=self.superadmin)

        # Verify
        result = nextcloud_group_manager.get_group(
            group_id=group_name, user=self.superadmin
        )
        self.assertEqual(result, None)

    @tag("nextcloud")
    def test_a_user_is_promoted_to_admin_and_creates_a_group(self):
        # Setup
        group_name = "test_group"
        # Promote User to Admin Group
        nextcloud_group_manager.promote_user_to_admin(
            user_id=self.user.get_nextcloud_user_id(),
            user=self.superadmin,
        )

        # Exercise
        nextcloud_group_manager.create_group(group_id=group_name, user=self.user)

        # Verify
        result = nextcloud_group_manager.get_group(
            group_id=group_name, user=self.superadmin
        )
        self.assertEqual(group_name, result)
        # Teardown
        nextcloud_group_manager.remove_group(group_id=group_name, user=self.superadmin)

    @tag("nextcloud")
    def test_a_user_is_promoted_to_admin_and_removes_a_group(self):
        # Setup
        group_name = "test_group"

        nextcloud_group_manager.promote_user_to_admin(
            user_id=self.user.get_nextcloud_user_id(),
            user=self.superadmin,
        )

        nextcloud_group_manager.create_group(group_id=group_name, user=self.user)

        # Exercise
        nextcloud_group_manager.remove_group(group_id=group_name, user=self.user)

        # Verify
        result = nextcloud_group_manager.get_group(
            group_id=group_name, user=self.superadmin
        )
        self.assertEqual(result, None)

    @tag("nextcloud")
    def test_add_user_to_group_successfully_adds_a_user_to_a_nextcloud_group(self):
        # Setup
        group_name = "test_group"
        user = user_fixture.create()
        nextcloud_user_manager.create_user(
            user_id=user.username,
            display_name=user.full_name,
            user_email=user.email,
            password=user.password,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )
        nc_app_password = nextcloud_user_manager.create_app_password(
            user.username, user.password
        )
        user.nc_app_password = nc_app_password
        user.save()

        nextcloud_group_manager.promote_user_to_admin(
            user_id=self.user.get_nextcloud_user_id(),
            user=self.superadmin,
        )
        nextcloud_group_manager.create_group(group_id=group_name, user=self.user)

        # Exercise
        nextcloud_group_manager.add_user_to_group(
            group_id=group_name, user_id=self.user.username, user=self.user
        )
        nextcloud_group_manager.add_user_to_group(
            group_id=group_name, user_id=user.username, user=self.user
        )

        # Verify
        group_members = nextcloud_group_manager.get_group_members(
            group_id=group_name, user=self.user
        )
        self.assertEqual(len(group_members), 2)
        self.assertTrue(group_members.__contains__(self.user.username))
        self.assertTrue(group_members.__contains__(user.username))

        # Teardown
        nextcloud_group_manager.remove_user_from_group(
            group_id=group_name, user_id=self.user.username, user=self.superadmin
        )
        nextcloud_group_manager.remove_user_from_group(
            group_id=group_name, user_id=user.username, user=self.superadmin
        )
        nextcloud_group_manager.remove_group(group_id=group_name, user=self.superadmin)

    @tag("nextcloud")
    def test_remove_from_group_successfully_removes_a_user_from_a_nextcloud_group(self):
        # Setup
        group_name = "test_group"
        user = user_fixture.create()
        nextcloud_user_manager.create_user(
            user_id=user.username,
            display_name=user.full_name,
            user_email=user.email,
            password=user.password,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )
        nc_app_password = nextcloud_user_manager.create_app_password(
            user.username, user.password
        )
        user.nc_app_password = nc_app_password
        user.save()
        nextcloud_group_manager.promote_user_to_admin(
            user_id=self.user.get_nextcloud_user_id(),
            user=self.superadmin,
        )
        nextcloud_group_manager.create_group(group_id=group_name, user=self.user)
        nextcloud_group_manager.add_user_to_group(
            group_id=group_name, user_id=self.user.username, user=self.user
        )
        nextcloud_group_manager.add_user_to_group(
            group_id=group_name, user_id=user.username, user=self.user
        )
        # Exercise
        nextcloud_group_manager.remove_user_from_group(
            group_id=group_name, user_id=self.user.username, user=self.superadmin
        )

        # Verify
        group_members = nextcloud_group_manager.get_group_members(
            group_id=group_name, user=self.user
        )
        self.assertEqual(len(group_members), 1)
        self.assertFalse(group_members.__contains__(self.user.username))
        self.assertTrue(group_members.__contains__(user.username))

        # Exercise
        nextcloud_group_manager.remove_user_from_group(
            group_id=group_name, user_id=user.username, user=self.superadmin
        )

        # Verify
        group_members = nextcloud_group_manager.get_group_members(
            group_id=group_name, user=self.user
        )
        self.assertEqual(len(group_members), 0)
        self.assertFalse(group_members.__contains__(self.user.username))
        self.assertFalse(group_members.__contains__(user.username))

        # Teardown
        nextcloud_group_manager.remove_group(group_id=group_name, user=self.superadmin)


class TestTalkResources(TestCase):
    fixtures = [
        "apps/accounts/seeds/02-groups.json",
        "apps/accounts/seeds/04-languages.json",
    ]

    def setUp(self):
        user_fixture.create_superadmin()
        self.superadmin = get_super_admin()
        self.user = user_fixture.create()
        self.admin_group = get_nextcloud_admin_group_name()
        nextcloud_user_manager.create_user(
            user_id=self.user.username,
            display_name=self.user.full_name,
            user_email=self.user.email,
            password=self.user.password,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )
        nc_app_password = nextcloud_user_manager.create_app_password(
            self.user.username, self.user.password
        )
        self.user.nc_app_password = nc_app_password
        self.user.save()
        nextcloud_group_manager.add_user_to_group(
            group_id=self.admin_group,
            user_id=self.user.username,
            user=self.superadmin,
        )
        self.group_name = nextcloud_group_manager.build_group_name(
            org_id=22, team_identifier=nextcloud_group_manager.GENERAL_GROUP_NAME
        )

        nextcloud_group_manager.create_group(group_id=self.group_name, user=self.user)

        nextcloud_group_manager.add_user_to_group(
            group_id=self.group_name, user_id=self.user.username, user=self.user
        )
        self.message = "Free Software"
        self.message_type = "comment"

    def tearDown(self):
        nextcloud_group_manager.remove_user_from_group(
            group_id=self.group_name,
            user_id=self.user.username,
            user=self.user,
        )
        nextcloud_group_manager.remove_group(group_id=self.group_name, user=self.user)

        nextcloud_group_manager.remove_user_from_group(
            group_id=self.admin_group,
            user_id=self.user.username,
            user=self.superadmin,
        )
        nextcloud_user_manager.delete_user(
            user_id=self.user.username,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )
        with self.assertRaises(BadRequest) as context:
            nextcloud_user_manager.get_user(
                user_id=self.user.username,
                auth_user_id=self.superadmin.get_nextcloud_user_id(),
                auth_user_app_password=self.superadmin.nc_app_password,
            )

    @tag("nextcloud")
    def test_create_group_conversation_successfully_creates_a_conversation_in_nextcloud(
        self,
    ):
        # Exercise
        nc_conversation = nextcloud_talk_manager.create_group_conversation(
            user_id=self.user.get_nextcloud_user_id(),
            group_id=self.group_name,
            room_name=self.group_name,
            user=self.user,
        )

        # Verify
        nc_conversation_token = nextcloud_utils.get_conversation_token(
            conversation=nc_conversation
        )
        self.assertTrue(nc_conversation_token)
        self.assertEqual(nc_conversation_token, nc_conversation["token"])
        self.assertEqual(self.group_name, nc_conversation["name"])
        self.assertEqual(nc_conversation["type"], ConversationType.group.value)

        # Teardown
        nextcloud_talk_manager.remove_conversation(
            conversation_token=nc_conversation_token, user=self.user
        )

    @tag("nextcloud")
    def test_create_group_conversation_fails_when_the_group_is_invalid(self):
        # Setup
        dummy_group_name = "dummy_name"

        # Exercise
        with self.assertRaises(ValidationError) as context:
            nextcloud_talk_manager.create_group_conversation(
                user_id=self.user.get_nextcloud_user_id(),
                group_id=dummy_group_name,
                room_name=self.group_name,
                user=self.user,
            )

    @tag("nextcloud")
    def test_get_user_conversations_sucessfully_returns_all_nextcloud_user_conversation(
        self,
    ):
        # Setup
        talk_updates_conversation_name = "Talk updates ✅"

        nextcloud_talk_manager.create_group_conversation(
            user_id=self.user.get_nextcloud_user_id(),
            group_id=self.group_name,
            room_name=self.group_name,
            user=self.user,
        )
        # Exercise
        nc_conversations = nextcloud_talk_manager.get_user_conversations(user=self.user)

        # Verify
        # All users are created with a default Talk conversation
        self.assertEqual(len(nc_conversations), 2)

        # New conversation
        group_conversation = nc_conversations[0]
        self.assertEqual(group_conversation["displayName"], self.group_name)
        self.assertEqual(group_conversation["type"], ConversationType.group.value)

        # Conversation created by default for each user
        talk_update_conversation = nc_conversations[1]
        self.assertEqual(
            talk_update_conversation["displayName"], talk_updates_conversation_name
        )
        self.assertEqual(
            talk_update_conversation["type"], ConversationType.changelog.value
        )

        # Teardown
        nextcloud_talk_manager.remove_conversation(
            conversation_token=nextcloud_utils.get_conversation_token(
                conversation=group_conversation
            ),
            user=self.user,
        )

    @tag("nextcloud")
    def test_get_user_conversations_fails_when_the_nextcloud_user_does_not_exist(self):
        # Setup
        user = user_fixture.create()
        # Exercise
        with self.assertRaises(ValidationError) as context:
            conversation = nextcloud_talk_manager.search_user_conversation(
                filter_value=self.group_name, user=user
            )

    @tag("nextcloud")
    def test_search_user_conversation_fails_when_the_group_id_is_invalid(self):
        nc_conversation = nextcloud_talk_manager.search_user_conversation(
            filter_value=self.group_name, user=self.user
        )
        self.assertEqual(nc_conversation, None)

    @tag("nextcloud")
    def test_search_user_conversation_sucessfully_get_specific_nextcloud_group_conversation(
        self,
    ):
        # Setup

        nextcloud_talk_manager.create_group_conversation(
            user_id=self.user.get_nextcloud_user_id(),
            group_id=self.group_name,
            room_name=self.group_name,
            user=self.user,
        )
        # Exercise
        nc_conversation = nextcloud_talk_manager.search_user_conversation(
            filter_value=self.group_name, user=self.user
        )

        # Verify
        self.assertEqual(nc_conversation["displayName"], self.group_name)
        self.assertEqual(nc_conversation["type"], ConversationType.group.value)

        # Teardown
        nextcloud_talk_manager.remove_conversation(
            conversation_token=nextcloud_utils.get_conversation_token(nc_conversation),
            user=self.user,
        )

    @tag("nextcloud")
    def test_get_conversation_messages_sucessfully_returns_messages_response_in_ascending_order(
        self,
    ):
        # Setup
        nc_conversation = nextcloud_talk_manager.create_group_conversation(
            user_id=self.user.get_nextcloud_user_id(),
            group_id=self.group_name,
            room_name=self.group_name,
            user=self.user,
        )
        nc_conversation_token = nextcloud_utils.get_conversation_token(nc_conversation)

        # Exercise
        data = nextcloud_talk_manager.get_conversation_messages(
            conversation_token=nc_conversation_token, user=self.user
        )

        # Verify
        messages = data["messages"]

        # Cast to int to compare
        last_given_id = int(data["last_given"])
        last_common_read = int(data["last_common_read"])

        older_message = messages[0]
        newer_message = messages[1]

        self.assertIsInstance(data, dict)
        self.assertIsInstance(messages, list)
        self.assertEqual(len(messages), 2)
        self.assertEqual(older_message["token"], nc_conversation_token)
        self.assertEqual(newer_message["token"], nc_conversation_token)
        self.assertTrue(older_message["id"] < newer_message["id"])
        self.assertEquals(newer_message["id"], last_given_id)
        self.assertEqual(older_message["id"], last_common_read)

        # Teardown
        nextcloud_talk_manager.remove_conversation(
            conversation_token=nc_conversation_token,
            user=self.user,
        )

    @tag("nextcloud")
    def test_get_conversation_messages_fails_when_conversation_token_is_invalid(self):
        # Setup
        expected_exception = ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_CONVERSATION_NOT_FOUND").data
        )

        nc_conversation = nextcloud_talk_manager.create_group_conversation(
            user_id=self.user.get_nextcloud_user_id(),
            group_id=self.group_name,
            room_name=self.group_name,
            user=self.user,
        )
        nc_conversation_token = nextcloud_utils.get_conversation_token(nc_conversation)

        invalid_nc_conversation_token = "dummytoken"

        # Exercise
        with self.assertRaises(ValidationError) as context:
            nextcloud_talk_manager.get_conversation_messages(
                conversation_token=invalid_nc_conversation_token, user=self.user
            )

        # Verify
        exception = context.exception
        self.assertEqual(exception, expected_exception)

        # Teardown
        nextcloud_talk_manager.remove_conversation(
            conversation_token=nc_conversation_token,
            user=self.user,
        )

    @tag("nextcloud")
    def test_get_conversation_messages_successfully_returns_messages_based_on_the_limit_argument(
        self,
    ):
        # Setup
        expected_length = 1
        nc_conversation = nextcloud_talk_manager.create_group_conversation(
            user_id=self.user.get_nextcloud_user_id(),
            group_id=self.group_name,
            room_name=self.group_name,
            user=self.user,
        )
        nc_conversation_token = nextcloud_utils.get_conversation_token(nc_conversation)

        # Exercise
        data = nextcloud_talk_manager.get_conversation_messages(
            conversation_token=nc_conversation_token,
            limit=expected_length,
            user=self.user,
        )

        # Verify
        messages = data["messages"]
        self.assertEqual(len(messages), expected_length)

        # Teardown
        nextcloud_talk_manager.remove_conversation(
            conversation_token=nc_conversation_token,
            user=self.user,
        )

    @tag("nextcloud")
    def test_get_conversation_messages_successfully_returns_a_messages_list_in_descending_order(
        self,
    ):
        # Setup
        nc_conversation = nextcloud_talk_manager.create_group_conversation(
            user_id=self.user.get_nextcloud_user_id(),
            group_id=self.group_name,
            room_name=self.group_name,
            user=self.user,
        )
        nc_conversation_token = nextcloud_utils.get_conversation_token(nc_conversation)

        # Exercise
        data = nextcloud_talk_manager.get_conversation_messages(
            conversation_token=nc_conversation_token,
            look_into_future=False,
            user=self.user,
        )

        # Verify
        messages = data["messages"]
        newer_message = messages[0]
        older_message = messages[1]

        # check based on the id that the first message is more current than the second message
        self.assertTrue(newer_message["id"] > older_message["id"])

        # Teardown
        nextcloud_talk_manager.remove_conversation(
            conversation_token=nc_conversation_token,
            user=self.user,
        )

    @tag("nextcloud")
    def test_send_message_to_conversation_successfully_sends_a_message(
        self,
    ):
        # Setup
        nc_conversation = nextcloud_talk_manager.create_group_conversation(
            user_id=self.user.get_nextcloud_user_id(),
            group_id=self.group_name,
            room_name=self.group_name,
            user=self.user,
        )
        nc_conversation_token = nextcloud_utils.get_conversation_token(nc_conversation)

        # Exercise
        response = nextcloud_talk_manager.send_message_to_conversation(
            conversation_token=nc_conversation_token,
            message=self.message,
            user=self.user,
        )

        # Verify
        nc_message = response["message"]
        self.assertEqual(nc_message["token"], nc_conversation_token)
        self.assertEqual(nc_message["message"], self.message)
        self.assertEqual(nc_message["messageType"], self.message_type)
        self.assertIsNotNone(nc_message["timestamp"])

        # Teardown
        nextcloud_talk_manager.remove_conversation(
            conversation_token=nc_conversation_token,
            user=self.user,
        )

    @tag("nextcloud")
    def test_send_message_to_conversation_fails_with_an_invalid_conversation_token(
        self,
    ):
        # Setup
        expected_exception = ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_USER_NOT_INCLUDED_IN_CONVERSATION").data
        )
        nc_conversation_token = "invalid_token"

        # Exercise
        with self.assertRaises(ValidationError) as context:
            nextcloud_talk_manager.send_message_to_conversation(
                conversation_token=nc_conversation_token,
                message=self.message,
                user=self.user,
            )

        # Verify
        exception = context.exception
        self.assertEqual(exception, expected_exception)

    @tag("nextcloud")
    def test_send_message_to_conversation_fails_when_the_user_does_not_belong_to_the_given_conversation(
        self,
    ):
        # Setup
        expected_exception = ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_USER_NOT_INCLUDED_IN_CONVERSATION").data
        )
        room_name = "another_room"
        team_identifer = "another_group"
        group_id = nextcloud_group_manager.build_group_name(
            org_id=team_identifer, team_identifier=team_identifer
        )
        nextcloud_group_manager.create_group(group_id=group_id, user=self.superadmin)

        nc_conversation = nextcloud_talk_manager.create_group_conversation(
            user_id=self.user.get_nextcloud_user_id(),
            group_id=group_id,
            room_name=room_name,
            user=self.superadmin,
        )
        nc_conversation_token = nextcloud_utils.get_conversation_token(nc_conversation)

        # Exercise
        with self.assertRaises(ValidationError) as context:
            nextcloud_talk_manager.send_message_to_conversation(
                conversation_token=nc_conversation_token,
                message=self.message,
                user=self.user,
            )

        # Verify
        exception = context.exception
        self.assertEqual(exception, expected_exception)

        # Teardown
        nextcloud_talk_manager.remove_conversation(
            conversation_token=nc_conversation_token, user=self.superadmin
        )
        nextcloud_group_manager.remove_group(group_id=group_id, user=self.superadmin)

    @tag("nextcloud")
    def test_send_message_to_conversation_fails_when_a_user_replies_to_an_unexistent_message(
        self,
    ):
        # Setup
        expected_exception = ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_SEND_MESSAGE_FAILED").data
        )
        invalid_id = "1"
        nc_conversation = nextcloud_talk_manager.create_group_conversation(
            user_id=self.user.get_nextcloud_user_id(),
            group_id=self.group_name,
            room_name=self.group_name,
            user=self.user,
        )
        nc_conversation_token = nextcloud_utils.get_conversation_token(nc_conversation)

        # Exercise
        with self.assertRaises(ValidationError) as context:
            nextcloud_talk_manager.send_message_to_conversation(
                conversation_token=nc_conversation_token,
                message=self.message,
                reply_to=invalid_id,
                user=self.user,
            )

        # Verify
        exception = context.exception
        self.assertEqual(exception, expected_exception)

    @tag("nextcloud")
    def test_send_message_to_conversation_succesfully_replies_to_a_message(self):
        # Setup
        nc_conversation = nextcloud_talk_manager.create_group_conversation(
            user_id=self.user.get_nextcloud_user_id(),
            group_id=self.group_name,
            room_name=self.group_name,
            user=self.user,
        )
        nc_conversation_token = nextcloud_utils.get_conversation_token(nc_conversation)

        response = nextcloud_talk_manager.send_message_to_conversation(
            conversation_token=nc_conversation_token,
            message=self.message,
            user=self.user,
        )
        nc_message = response["message"]

        # Exercise
        new_response = nextcloud_talk_manager.send_message_to_conversation(
            conversation_token=nc_conversation_token,
            message=self.message,
            user=self.user,
            reply_to=nc_message["id"],
        )

        # Verify
        nc_message_reply = new_response["message"]
        self.assertEqual(nc_message_reply["token"], nc_conversation_token)
        self.assertEqual(nc_message_reply["message"], self.message)
        self.assertEqual(nc_message_reply["messageType"], self.message_type)
        self.assertIsNotNone(nc_message_reply["timestamp"])
        self.assertEqual(nc_message_reply["parent"]["id"], nc_message["id"])

        # Teardown
        nextcloud_talk_manager.remove_conversation(
            conversation_token=nc_conversation_token,
            user=self.user,
        )


class TestFilesResources(TestCase):
    fixtures = [
        "apps/accounts/seeds/02-groups.json",
        "apps/accounts/seeds/04-languages.json",
    ]

    def setUp(self):
        user_fixture.create_superadmin()
        self.superadmin = get_super_admin()
        self.user = user_fixture.create()
        self.admin_group = get_nextcloud_admin_group_name()
        nextcloud_user_manager.create_user(
            user_id=self.user.username,
            display_name=self.user.full_name,
            user_email=self.user.email,
            password=self.user.password,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )
        nc_app_password = nextcloud_user_manager.create_app_password(
            self.user.username, self.user.password
        )
        self.user.nc_app_password = nc_app_password
        self.user.save()
        nextcloud_group_manager.add_user_to_group(
            group_id=self.admin_group,
            user_id=self.user.username,
            user=self.superadmin,
        )
        language = Language.objects.get(id=1)
        self.organization = organization_fixture.create({"language": language})
        self.group_name = nextcloud_group_manager.build_group_name(
            org_id=self.organization.id,
            team_identifier=nextcloud_group_manager.GENERAL_GROUP_NAME,
        )

        nextcloud_group_manager.create_group(group_id=self.group_name, user=self.user)

        nextcloud_group_manager.add_user_to_group(
            group_id=self.group_name, user_id=self.user.username, user=self.user
        )
        self.file_name = "testblue.jpg"
        data = create_dummy_image(self.file_name)
        self.image_file = data.get("image")
        self.full_file_path = data.get("path")

    def tearDown(self):
        nextcloud_group_manager.remove_user_from_group(
            group_id=self.group_name,
            user_id=self.user.username,
            user=self.user,
        )
        nextcloud_group_manager.remove_group(group_id=self.group_name, user=self.user)

        nextcloud_group_manager.remove_user_from_group(
            group_id=self.admin_group,
            user_id=self.user.username,
            user=self.superadmin,
        )
        nextcloud_user_manager.delete_user(
            user_id=self.user.username,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )
        with self.assertRaises(BadRequest) as context:
            nextcloud_user_manager.get_user(
                user_id=self.user.username,
                auth_user_id=self.superadmin.get_nextcloud_user_id(),
                auth_user_app_password=self.superadmin.nc_app_password,
            )
        self.image_file.close()

    @tag("nextcloud")
    def test_list_conversation_files_returns_an_empty_list_when_there_are_no_shared_files(
        self,
    ):
        # Setup
        nc_files_response = {
            "files": [],
        }
        nc_conversation = nextcloud_talk_manager.create_group_conversation(
            user_id=self.user.get_nextcloud_user_id(),
            group_id=self.group_name,
            room_name=self.group_name,
            user=self.user,
        )
        nc_conversation_token = nextcloud_utils.get_conversation_token(nc_conversation)
        # Exercise
        nc_files = nextcloud_files_manager.list_conversation_files(
            conversation_token=nc_conversation_token, user=self.user
        )
        # Verify
        self.assertEqual(nc_files, nc_files_response)
        # Teardown
        nextcloud_talk_manager.remove_conversation(
            conversation_token=nc_conversation_token,
            user=self.user,
        )

    @tag("nextcloud")
    def test_list_conversation_files_fails_when_the_conversation_token_is_invalid(
        self,
    ):
        # Setup
        nc_conversation_token = "dummy_token"
        # Exercise and Verify
        with self.assertRaises(ValidationError) as context:
            nextcloud_files_manager.list_conversation_files(
                conversation_token=nc_conversation_token, user=self.user
            )

    @tag("nextcloud")
    def test_upload_successfully_uploads_an_image(self):
        # Setup
        user_files = nextcloud_files_manager.list_files(user=self.superadmin)
        destination_path = f"{get_talk_dir()}/{self.file_name}"
        content_type = "image/jpeg"
        path = f"/remote.php/dav/files/{self.superadmin.get_nextcloud_user_id()}{destination_path}"

        # Exercise
        nextcloud_files_manager.upload(
            self.full_file_path, self.file_name, self.superadmin
        )

        # Verify
        user_new_files = nextcloud_files_manager.list_files(user=self.superadmin)
        self.assertEquals(len(user_files) + 1, len(user_new_files))

        last_file = user_files[-1]
        new_file = user_new_files[-1]
        self.assertNotEquals(last_file, new_file)
        self.assertEqual(path, new_file.get("d:href"))
        self.assertEqual(
            content_type,
            new_file.get("d:propstat")[0].get("d:prop").get("d:getcontenttype"),
        )

        # Teardown
        nextcloud_files_manager.delete_file(
            f"{get_talk_dir()}/{self.file_name}", self.superadmin
        )

    @tag("nextcloud")
    def test_upload_fails_when_the_file_does_not_exist(self):
        # Setup
        file_name = "dummyfile.jpg"
        file_path = settings.MEDIA_ROOT + file_name
        expected_exception = ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_FILE_NOT_FOUND").data
        )
        # Exercise
        with self.assertRaises(ValidationError) as context:
            nextcloud_files_manager.upload(file_path, file_name, self.superadmin)
        exception = context.exception

        self.assertEqual(exception, expected_exception)

    @tag("nextcloud")
    def test_create_conversation_share_successfully_shares_a_file_in_conversation(self):
        # Setup
        expected_length = 1
        user_files = nextcloud_files_manager.list_files(user=self.superadmin)
        talk_path = f"{get_talk_dir()}/{self.file_name}"
        content_type = "image/jpeg"
        nextcloud_files_manager.upload(
            self.full_file_path, self.file_name, self.superadmin
        )

        nextcloud_group_manager.add_user_to_group(
            group_id=self.group_name, user_id=self.superadmin.username, user=self.user
        )
        nc_conversation = nextcloud_talk_manager.create_group_conversation(
            user_id=self.user.get_nextcloud_user_id(),
            group_id=self.group_name,
            room_name=self.group_name,
            user=self.user,
        )
        nc_conversation_token = nextcloud_utils.get_conversation_token(nc_conversation)

        # Exercise
        nextcloud_shares_manager.create_conversation_share(
            talk_path, nc_conversation_token, self.superadmin
        )

        # Verify
        response = nextcloud_files_manager.list_conversation_files(
            conversation_token=nc_conversation_token, user=self.superadmin
        )
        shared_files = response["files"]
        shared_file = shared_files[0]

        self.assertEqual(len(shared_files), expected_length)
        self.assertIn("path", shared_file.get("messageParameters").get("file"))
        self.assertEqual(
            "/" + shared_file.get("messageParameters").get("file").get("path"),
            talk_path,
        )
        self.assertIn("name", shared_file.get("messageParameters").get("file"))
        self.assertEqual(
            shared_file.get("messageParameters").get("file").get("name"), self.file_name
        )
        self.assertIn("actorId", shared_file)
        self.assertEqual(
            shared_file["actorId"], self.superadmin.get_nextcloud_user_id()
        )

        # Teardown
        nextcloud_files_manager.delete_file(talk_path, self.superadmin)
        nextcloud_talk_manager.remove_conversation(
            conversation_token=nc_conversation_token, user=self.user
        )

    @tag("nextcloud")
    def test_get_share_successfully_returns_a_file_details(self):
        # Setup
        talk_path = f"{get_talk_dir()}/{self.file_name}"
        content_type = "image/jpeg"
        nextcloud_files_manager.upload(
            self.full_file_path, self.file_name, self.superadmin
        )

        nextcloud_group_manager.add_user_to_group(
            group_id=self.group_name, user_id=self.superadmin.username, user=self.user
        )
        nc_conversation = nextcloud_talk_manager.create_group_conversation(
            user_id=self.user.get_nextcloud_user_id(),
            group_id=self.group_name,
            room_name=self.group_name,
            user=self.user,
        )
        nc_conversation_token = nextcloud_utils.get_conversation_token(nc_conversation)

        response = nextcloud_shares_manager.create_conversation_share(
            talk_path, nc_conversation_token, self.superadmin
        )

        # Exercise
        share_id = response.get("id")
        shared_nextcloud_file = nextcloud_shares_manager.get_share(share_id, self.user)

        # Verify
        self.assertIn("uid_owner", shared_nextcloud_file)
        self.assertEqual(
            shared_nextcloud_file.get("uid_owner"),
            self.superadmin.get_nextcloud_user_id(),
        )
        self.assertIn("share_with_displayname", shared_nextcloud_file)
        self.assertEqual(
            shared_nextcloud_file.get("share_with_displayname"), self.group_name
        )
        self.assertIn("path", shared_nextcloud_file)
        self.assertEqual(shared_nextcloud_file.get("path"), talk_path)

        # Teardown
        nextcloud_files_manager.delete_file(
            f"{get_talk_dir()}/{self.file_name}", self.superadmin
        )

    @tag("nextcloud")
    def test_get_share_fails_when_the_share_id_is_not_valid(self):
        # Setup
        dummy_id = 9999
        expected_exception = ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_GET_SHARE_ERROR").data
        )
        # Exercise
        with self.assertRaises(ValidationError) as context:
            shared_nextcloud_file = nextcloud_shares_manager.get_share(
                dummy_id, self.user
            )
        exception = context.exception

        self.assertEqual(exception, expected_exception)
