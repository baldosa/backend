"""
Conveniece facade module for nextcloud operations
"""

import logging

from apps.accounts.exceptions import InvalidPasswordException
from apps.accounts.models import User, get_super_admin

from apps.nextcloud.resources import files as nextcloud_files_manager
from apps.nextcloud.resources import groups as nextcloud_group_manager
from apps.nextcloud.resources import talk as nextcloud_talk_manager
from apps.nextcloud.resources import users as nextcloud_user_manager
from apps.nextcloud.resources import utils as nextcloud_utils
from apps.nextcloud.occ import AppPasswordException

from .exceptions import (
    NextcloudConversationCreationException,
    NextcloudPasswordCreationException,
    NextcloudUserNotFound,
    NextcloudVeryCommonPasswordException,
)

logger = logging.getLogger(__name__)


def create_app_password(username: str, password: str) -> str:
    """
    Given a username and a password, creates an app password
    """
    return nextcloud_user_manager.create_app_password(username, password)


def initialise_user(username: str, password: str, user: User) -> User:
    """
    Given a username, a password and a user model, initialises the user in
    nextcloud.
    """
    superadmin_user: User = get_super_admin()

    # TODO: create user in nextcloud: use the app password of a
    # privileged account
    try:
        nextcloud_user_manager.create_user(
            user_id=username,
            display_name=user.full_name,
            user_email=user.email,
            password=password,
            auth_user_id=superadmin_user.get_nextcloud_user_id(),
            auth_user_app_password=superadmin_user.nc_app_password,
        )
    except NextcloudVeryCommonPasswordException as e:
        raise InvalidPasswordException from e
    except Exception as e:
        logger.error(
            "Unexpected error while creation nextcloud app password %s", str(e)
        )
        raise NextcloudPasswordCreationException from e

    # Checks that the user was created successfully
    if nextcloud_user_manager.user_exists_in_nextcloud(
        username,
        superadmin_user.get_nextcloud_user_id(),
        superadmin_user.nc_app_password,
    ):
        # Create app password for the new user
        try:
            nc_app_password: str = create_app_password(username, password)
        except AppPasswordException as e:
            nextcloud_user_manager.delete_user(
                user_id=username,
                auth_user_id=superadmin_user.get_nextcloud_user_id(),
                auth_user_app_password=superadmin_user.nc_app_password,
            )
            raise NextcloudPasswordCreationException from e
    else:
        raise NextcloudUserNotFound

    user.username = username
    user.nc_app_password = nc_app_password
    user.save()

    return user


def initialise_talk(user: User, nextcloud_user_id: str) -> None:
    """
    Given a user initialises the talk directory
    """
    nextcloud_files_manager.create_talk_folder(user, nextcloud_user_id)


def initialise_projects(user: User, nextcloud_user_id: str) -> None:
    """
    Given a user initialises the projects directory
    """
    nextcloud_files_manager.create_projects_folder(user, nextcloud_user_id)


def initialise_conversation(group_name, nextcloud_user_id, user, auth_user):
    """
    Given a group name, a nextcloud user id, and a user initialises a new group,
    assigning the proper permissions to the caller.
    """
    try:
        # Add user in group
        nextcloud_group_manager.add_user_to_group(
            group_id=group_name,
            user_id=nextcloud_user_id,
            user=auth_user,
        )
        # Provides conversation creation privileges to the Admin
        nextcloud_group_manager.promote_user_to_subadmin(
            group_id=group_name,
            user_id=nextcloud_user_id,
            user=auth_user,
        )
        # Create a conversation group
        nc_conversation = nextcloud_talk_manager.create_group_conversation(
            user_id=nextcloud_user_id,
            room_name=group_name,
            group_id=group_name,
            user=user,
        )
        return nextcloud_utils.get_conversation_token(nc_conversation)

    except Exception as e:
        raise NextcloudConversationCreationException from e
