"""
This module provides a direct way of interacting with the Nextcloud files API.
"""

import asyncio
import logging

from nextcloud_async import exceptions as nextcloud_exceptions
from django.core.exceptions import ValidationError
from colmena.serializers.serializers import ErrorSerializer

from apps.accounts.models import User
from apps.nextcloud.config import get_talk_dir, get_projects_dir
from apps.nextcloud.exceptions import (
    NextcloudUploadFileException,
    NextcloudDownloadFileException,
)
from apps.nextcloud.resources.auth import auth
from apps.nextcloud.resources import utils as nextcloud_util

logger = logging.getLogger(__name__)

TALK_DIR = get_talk_dir()
PROJECTS_DIR = get_projects_dir()


def list_files(user=None):
    """
    Given a user to authenticate the request, returns all nextcloud files for the given user.

    Parameters
    ----------

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        [
            {
                "d:href": String,
                "d:propstat": [
                    {
                        "d:prop": {
                            "nc:sharees": {
                                "nc:sharee": {
                                    "nc:id": String,
                                    "nc:display-name": String,
                                    "nc:type": Enum,
                                }
                            },
                            "oc:owner-id": String,
                            "oc:fileid": String,
                            "oc:share-types": {"oc:share-type": Enum},
                            "d:getlastmodified": Date,
                            "d:getcontentlength": String,
                            "d:resourcetype": Enum,
                            "d:getetag": String,
                            "d:getcontenttype": Enum,
                        },
                        "d:status": Enum,
                    },
                    {"d:prop": {"d:displayName": String}, "d:status": String}
                ]
            }
        ]


    Raises
    ------
    ValidationError
        When the nextcloud server answers unexpectedly.
    """
    try:
        return _list_files_in_folder(TALK_DIR, user)
    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed fetching list of files from user", extra={"auth_user_id": user.id}
        )
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_GET_FILES_FAILED").data
        ) from e


def list_projects(user: User):
    """
    Given a user to authenticate the request, returns all nextcloud projects for the given user.

    Parameters
    ----------

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        [
            {
                "d:href": String,
                "d:propstat": [
                    {
                        "d:prop": {
                            "nc:sharees": {
                                "nc:sharee": {
                                    "nc:id": String,
                                    "nc:display-name": String,
                                    "nc:type": Enum,
                                }
                            },
                            "oc:owner-id": String,
                            "oc:fileid": String,
                            "oc:share-types": {"oc:share-type": Enum},
                            "d:getlastmodified": Date,
                            "d:getcontentlength": String,
                            "d:resourcetype": Enum,
                            "d:getetag": String,
                            "d:getcontenttype": Enum,
                        },
                        "d:status": Enum,
                    },
                    {"d:prop": {"d:displayName": String}, "d:status": String}
                ]
            }
        ]


    Raises
    ------
    ValidationError
        When the nextcloud server answers unexpectedly.
    """
    try:
        return _list_files_in_folder(PROJECTS_DIR, user)
    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed fetching list of projects from user",
            extra={"auth_user_id": user.id},
        )
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_GET_PROJECTS_FAILED").data
        ) from e


def list_conversation_files(
    conversation_token=None, user=None, limit=100, descending=True
):
    """
    Given a user to authenticate the request, returns the last 100 files and media attached to the given conversation in descending order.

    Parameters
    ----------

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        {
            "audio": [],
            "deckcard": [],
            "file": [
                {
                    "id": Number,
                    "token": String,
                    "actorType": Enum,
                    "actorId": String,
                    "actorDisplayName": String,
                    "timestamp": timestamp,
                    "message": String,
                    "messageParameters": {
                        "actor": {
                            "type": String,
                            "id": String,
                            "name": String
                        },
                        "file": {
                            "type": "file",
                            "id": String,
                            "name": String,
                            "size": Number,
                            "path": String,
                            "link": String,
                            "etag": String,
                            "permissions": Enum,
                            "mimetype": Enum,
                            "preview-available": String
                        }
                    },
                    "systemMessage": String,
                    "messageType":Enum,
                    "isReplyable": Bool,
                    "referenceId": String,
                    "reactions": Dict,
                    "expirationTimestamp": Number
                },
            ],
            "location": [],
            "media": [],
            "other": [],
            "poll": [],
            "voice": [],
        }

    Raises
    ------
    ValidationError
        When the conversation_token is not a valid value.
    """
    nextcloud_util.validate_conversation_token(conversation_token)
    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)

    try:
        response = asyncio.run(
            # The limit is hardcoded to the last 100 elements of each type.
            nextcloud_async.get_shared_items_overview(conversation_token, limit=100)
        )
        # Concatenate files and media elements and sort by timestamp in ascending or descending order
        data = {
            "files": sorted(
                response["file"] + response["media"] + response["audio"],
                key=lambda x: x["timestamp"],
                reverse=descending,
            )[:limit]
        }
        data["files"] = _filter_system_messages(data["files"])
        return data
    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed fetching list of files from user conversation",
            extra={"auth_user_id": user.id, "conversation_token": conversation_token},
        )
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_GET_CONVERSATION_FILES_FAILED").data
        ) from e


def create_talk_folder(user, username):
    """
    Given a user to authenticate the request, creates a default talk folder.

    Parameters
    ----------

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    username : str
        Represents the username to be assigned to the nextcloud account.

    Returns
    -------
       None

    Raises
    ------
    ValidationError
        When the nextcloud server answers unexpectedly.
    """

    try:
        _create_folder(user, username, TALK_DIR)
    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed creating default talk folder",
            extra={"auth_user_id": user.id, "username": username},
        )
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_CREATION_FOLDER_FAILED").data
        ) from e


def create_projects_folder(user, username):
    """
    Given a user to authenticate the request, creates a default projects folder.

    Parameters
    ----------

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    username : str
        The nextcloud username to assign the folder to.

    Returns
    -------
       None

    Raises
    ------
    ValidationError
        When the nextcloud server answers unexpectedly.
    """

    try:
        _create_folder(user, username, PROJECTS_DIR)
    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed creating default project folder",
            extra={"auth_user_id": user.id, "username": username},
        )
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_CREATION_FOLDER_FAILED").data
        ) from e


def upload(file_path: str, filename: str, remote_path: str, user):
    """
    Given a file path, a filename and a user to authenticate the request,
    uploads a file in the file path to the default Talk folder.

    Parameters
    ----------

    file_path : str
       A file path

    filename : str
        A filename

    remote_path : str
        A remote path

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        When the conversation_token is not a valid value.
    """
    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)

    try:
        asyncio.run(
            nextcloud_async.upload_file(local_path=file_path, remote_path=remote_path)
        )
    except FileNotFoundError as e:
        logger.error(
            "Failed finding file",
            extra={
                "auth_user_id": user.id,
                "file_path": file_path,
                "filename": filename,
            },
        )
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_FILE_NOT_FOUND").data
        ) from e
    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed uploading file",
            extra={
                "auth_user_id": user.id,
                "file_path": file_path,
                "filename": filename,
            },
        )
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_FILE_UPLOAD_FAILED").data
        ) from e
    except Exception as e:
        logger.error(
            "Failed uploading file",
            extra={
                "auth_user_id": user.id,
                "file_path": file_path,
                "filename": filename,
            },
        )
        raise NextcloudUploadFileException from e


def delete_file(file_path, user):
    """
    Given a file path and a user to authenticate the request removes the file

    Parameters
    ----------

    file_path : str
       A file path

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        When the file path is not a valid value.
    """
    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)
    try:
        asyncio.run(nextcloud_async.delete(path=file_path))

    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed deleting file",
            extra={
                "auth_user_id": user.id,
                "file_path": file_path,
            },
        )
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_DELETION_ERROR").data
        ) from e


def download_file(file_path, user):
    """
    Given a file path and a user to authenticate the request download the file.

    Parameters
    ----------

    file_path : str
       A file path

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        ByteString

    Raises
    ------
    ValidationError
        When the file path is not a valid value.
    """

    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)
    try:
        return asyncio.run(nextcloud_async.download_file(path=file_path))

    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed downloading file",
            extra={
                "auth_user_id": user.id,
                "file_path": file_path,
            },
        )
        raise NextcloudDownloadFileException from e


def _filter_system_messages(messages):
    return list(filter(lambda message: _is_file_message(message), messages))


def _is_file_message(message):
    return message["messageType"] == "comment" and message["message"] == "{file}"


def _create_folder(user, username, folder_path):
    """
    Given a user to authenticate the request, creates a default project folder.

    Parameters
    ----------

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    username : str
        Represents the username to be assigned to the nextcloud account.

    folder_path : str
       A file path


    Returns
    -------
       None

    Raises
    ------
    ValidationError
        When the nextcloud server answers unexpectedly.
    """
    nextcloud_async = auth(username, user.nc_app_password)

    asyncio.run(nextcloud_async.create_folder(folder_path))


def _list_files_in_folder(nextcloud_folder, user):
    props = [
        "d:displayName",
        "nc:sharees",
        "oc:owner-id",
        "oc:fileid",
        "oc:share-types",
        "d:getlastmodified",
        "d:getcontentlength",
        "d:resourcetype",
        "d:getetag",
        "d:getcontenttype",
    ]
    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)
    try:
        response = asyncio.run(nextcloud_async.list_files(nextcloud_folder, props))
        # Handles the case where there is only one item to be added to a list.
        if isinstance(response, dict):
            response = [response]

        return response
    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed fetching list of files from user",
            extra={"auth_user_id": user.id, "nextcloud_folder": nextcloud_folder},
        )
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_GET_FILES_FAILED").data
        ) from e
