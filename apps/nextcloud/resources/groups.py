import asyncio

from apps.nextcloud.config import get_nextcloud_admin_group_name
from apps.nextcloud.exceptions import (
    NextcloudAddUserToGroupException,
    NextcloudGroupCreationException,
)
from apps.nextcloud.resources.auth import auth
from apps.nextcloud.resources.utils import (
    validate_group_id,
    validate_user_id,
)
import logging

logger = logging.getLogger(__name__)

from colmena.serializers.serializers import ErrorSerializer

from django.core.exceptions import ValidationError

from nextcloud_async import exceptions as nextcloud_exceptions

GENERAL_GROUP_NAME = "general"


def validate_not_admin_group(group_id):
    if group_id == get_nextcloud_admin_group_name():
        raise ValidationError(ErrorSerializer("ERRORS_NEXTCLOUD_GROUP_EXIST").data)


def build_group_name(org_id=None, team_identifier=None):
    return f"organization-{org_id}-{team_identifier}"


def build_personal_workspace_name(user_id):
    return f"{user_id}-personal-workspace"


def create_group(group_id=None, user=None):
    """
    Given a group_id and a user to authenticate the request, creates a new group in nextcloud with given group_id.

    Parameters
    ----------
    group_id : str
        The nextcloud group id

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        When the group_id is not a valid value.
    """
    validate_group_id(group_id)
    validate_not_admin_group(group_id)

    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)

    try:
        asyncio.run(nextcloud_async.create_group(group_id))
    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed creating group",
            extra={
                "auth_user_id": user.id,
                "group_id": group_id,
            },
        )
        raise NextcloudGroupCreationException


def remove_group(group_id=None, user=None):
    """
    Given a group_id and a user to authenticate the request, removes the group with the given group_id.

    Parameters
    ----------
    group_id : str
        The nextcloud group id

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        If the group_id does not exist
    """
    validate_group_id(group_id)
    validate_not_admin_group(group_id)

    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)
    try:
        asyncio.run(nextcloud_async.remove_group(group_id))
    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed removing group",
            extra={
                "auth_user_id": user.id,
                "group_id": group_id,
            },
        )
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_GROUP_DELETED_ERROR").data
        )


def get_group(group_id=None, user=None):
    """
    Given a group_id and a user to authenticate the request, returns the group_id or None if the group does not exist.

    Parameters
    ----------
    group_id : str
        The nextcloud group id

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        The group_id or None if the group does not exist.

    Raises
    ------
    ValidationError
        When group_id does not exist
    """

    validate_group_id(group_id)

    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)
    try:
        group = asyncio.run(nextcloud_async.search_groups(search=group_id, limit=1))
    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed getting group",
            extra={
                "auth_user_id": user.id,
                "group_id": group_id,
            },
        )
        raise ValidationError(ErrorSerializer("ERRORS_NEXTCLOUD_GET_GROUP_FAILED").data)
    if group:
        return group[0]


def add_user_to_group(group_id=None, user_id=None, user=None):
    """
    Given a group_id, a user_id and a user to authenticate the request, adds the user with user_id in the given group or raises if the request failed.

    Parameters
    ----------
    group_id : str
        The nextcloud group id

    user_id : str
        The nextcloud user name

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        When either the user_id or the group_id don't exist in nextcloud.
    """
    validate_group_id(group_id)
    validate_user_id(user_id)

    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)
    try:
        asyncio.run(
            nextcloud_async.add_user_to_group(user_id=user_id, group_id=group_id)
        )
    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed adding new member to group",
            extra={"auth_user_id": user.id, "group_id": group_id, "user_id": user_id},
        )
        raise NextcloudAddUserToGroupException


def remove_user_from_group(group_id=None, user_id=None, user=None):
    """
    Given a group_id, a user_id and a user to authenticate the
    request removes the given user id from the given group or
    raises.

    Parameters
    ----------
    group_id : str
        The nextcloud group id

    user_id : str
        The nextcloud user name

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        When the user_id does not belong to the given group_id
    """

    validate_group_id(group_id)
    validate_user_id(user_id)

    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)
    try:
        asyncio.run(
            nextcloud_async.remove_user_from_group(user_id=user_id, group_id=group_id)
        )
    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed removing member in group",
            extra={"auth_user_id": user.id, "group_id": group_id, "user_id": user_id},
        )
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_REMOVE_USER_TO_GROUP_FAILED").data
        )


def get_group_members(group_id=None, user=None):
    """
    Given a group_id and a user to authenticate the request, returns
    a list of nextcloud users.

    Parameters
    ----------
    group_id : str
        The nextcloud group id

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        ['some_user_id', 'some_other_user_id']

    Raises
    ------
    ValidationError
        If the members list cannot be retrieved
    """

    validate_group_id(group_id)

    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)
    try:
        return asyncio.run(nextcloud_async.get_group_members(group_id=group_id))
    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed getting group members",
            extra={
                "auth_user_id": user.id,
                "group_id": group_id,
            },
        )
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_FETCH_GROUP_MEMBERS_FAILED").data
        )


def promote_user_to_admin(user_id=None, user=None):
    """
    Promotes the given user as a nextcloud admin by adding the user_id to the admin group

    Parameters
    ----------
    user_id : str
         The nextcloud user name

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        When either user_id or group_id do not exist
    """

    validate_user_id(user_id)

    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)
    try:
        asyncio.run(
            nextcloud_async.add_user_to_group(
                user_id=user_id, group_id=get_nextcloud_admin_group_name()
            )
        )
    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed adding member to admin group",
            extra={
                "auth_user_id": user.id,
                "user_id": user_id,
            },
        )
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_PROMOTE_USER_TO_ADMIN_FAILED").data
        )


def promote_user_to_subadmin(user_id=None, group_id=None, user=None):
    """
    Promotes the given user as a group admin by adding the user_id to the subadmin role

    Parameters
    ----------
    user_id : str
         The nextcloud user name

    group_id : str
        The nextcloud group id

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        When either user_id or group_id do not exist
    """

    validate_user_id(user_id)
    validate_group_id(group_id)

    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)
    try:
        asyncio.run(
            nextcloud_async.promote_user_to_subadmin(user_id=user_id, group_id=group_id)
        )
    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed promoting member to sub-admin",
            extra={"auth_user_id": user.id, "user_id": user_id, "group_id": group_id},
        )
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_PROMOTE_USER_TO_SUBADMIN_FAILED").data
        )
