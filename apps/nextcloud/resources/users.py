import asyncio
import logging
import re

from nextcloud_async import exceptions as nextcloud_exceptions

from apps.nextcloud.resources.auth import auth
from apps.nextcloud import occ
from apps.nextcloud.exceptions import (
    NextcloudVeryCommonPasswordException,
    NextcloudUnexpectedUserCreationException,
)

from colmena.settings.base import NEXTCLOUD_DEFAULT_USER_QUOTA

from django.core.exceptions import BadRequest


logger = logging.getLogger(__name__)

NEXTCLOUD_ERROR_WITH_HINT_STATUS_CODE = 107
NEXTCLOUD_WEAK_PASSWORD_PATTERN = "1,000,000"


def create_user(
    user_id=None,
    display_name=None,
    user_email=None,
    password=None,
    auth_user_id=None,
    auth_user_app_password=None,
):
    nextcloudAsync = auth(auth_user_id, auth_user_app_password)
    try:
        asyncio.run(
            nextcloudAsync.create_user(
                user_id=user_id,
                display_name=display_name,
                email=user_email,
                quota=NEXTCLOUD_DEFAULT_USER_QUOTA,
                ##FIXME : A default value is used because there is no real impact at this time.
                language="en",
                password=password,
            )
        )
    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed creating user",
            extra={
                "user_id": user_id,
                "user_email": user_email,
                "display_name": display_name,
                "auth_user_id": auth_user_id,
            },
        )
        # HINT: The exception status code is checked. Nextcloud supports multiple languages,
        # it checks that the exception message contains the number 1,000,000 which is unchanged
        # in most translations.
        if (e.status_code == NEXTCLOUD_ERROR_WITH_HINT_STATUS_CODE) and re.search(
            NEXTCLOUD_WEAK_PASSWORD_PATTERN, e.reason
        ):
            raise NextcloudVeryCommonPasswordException from e
        try:
            # Rollback user creation in case of errors
            delete_user(
                user_id=user_id,
                auth_user_id=auth_user_id,
                auth_user_app_password=auth_user_app_password,
            )
        except Exception as deletion_exception:
            logger.error(
                "Failed deleting user on create_user rollback",
                extra={
                    "user_id": user_id,
                    "user_email": user_email,
                    "auth_user_id": auth_user_id,
                },
            )
            e = deletion_exception

        raise NextcloudUnexpectedUserCreationException from e


def get_user(user_id=None, auth_user_id=None, auth_user_app_password=None):
    nextcloudAsync = auth(auth_user_id, auth_user_app_password)
    try:
        user = asyncio.run(nextcloudAsync.get_user(user_id))

    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed getting user",
            extra={"user_id": user_id, "auth_user_id": auth_user_id},
        )
        raise BadRequest("ERRORS_NEXTCLOUD_USER_NOT_FOUND")

    return user


def delete_user(user_id=None, auth_user_id=None, auth_user_app_password=None):
    nextcloudAsync = auth(auth_user_id, auth_user_app_password)
    try:
        asyncio.run(nextcloudAsync.remove_user(user_id))
    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed deleting user",
            extra={"user_id": user_id, "auth_user_id": auth_user_id},
        )
        raise BadRequest("ERRORS_NEXTCLOUD_USER_DELETION_FAILED")


def create_app_password(user_id=None, user_password=None):
    return occ.create_app_password(user_id, user_password)


def user_exists_in_nextcloud(user_id, auth_user_id, auth_user_app_password):
    try:
        return (
            user_id
            == get_user(
                user_id=user_id,
                auth_user_id=auth_user_id,
                auth_user_app_password=auth_user_app_password,
            )["id"]
        )

    except Exception as e:
        raise BadRequest("ERRORS_NEXTCLOUD_GET_USER_FAILED")


def update_user(
    updated_data: dict, user_id: str, auth_user_id: str, auth_user_app_password: str
):
    nextcloudAsync = auth(auth_user_id, auth_user_app_password)
    try:
        asyncio.run(nextcloudAsync.update_user(user_id, updated_data))
    except nextcloud_exceptions.NextCloudException as e:
        logger.error(
            "Failed updating user",
            extra={
                "user_id": user_id,
                "auth_user_id": auth_user_id,
                "update_data": updated_data,
            },
        )
        raise BadRequest("ERRORS_NEXTCLOUD_UPDATE_USER_FAILED")
