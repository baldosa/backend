# TODO: fix problem with django_aloe
aloe_django
colorlog==6.8.0
Django==4.2.3
dj-rest-auth>=3.0.0
django-avatar
django-cors-headers==4.2.0
django-countries==7.5.1
django-extensions==3.2.3
django-password-validators==1.7.1
djangorestframework==3.14.0
djangorestframework-simplejwt==5.2.2
django-templated-email==3.0.1
drf-extensions==0.7.1
drf-extra-fields==3.7.0
drf-spectacular==0.26.3
json-log-formatter==0.5.1
markdown
nextcloud-api-wrapper==0.2.3
nextcloud-async @ git+https://github.com/Cambalab/nextcloud-async
passlib
psycopg2-binary==2.9.6
pyyaml<=5.3.1
requests==2.31.0
openapi-python-generator==0.4.8