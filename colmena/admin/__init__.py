from django.utils.html import mark_safe


def generate_preview(image_field):
    return mark_safe(
        f'<img src="{image_field.url}" width="{image_field.width}" height={image_field.height} />'
    )
