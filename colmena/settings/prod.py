import dj_database_url
import os
from .base import *


# ------------------------------------------------------------------------------
# Application Settings
#
WSGI_APPLICATION = "colmena.wsgi.application"
DEBUG = bool(int(os.environ.get("DEBUG", 0)))

# ------------------------------------------------------------------------------
# Logger Settings
#

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        },
        "file": {
            "level": "ERROR",
            "class": "logging.FileHandler",
            "filename": "logs/error.log",
            "formatter": "verbose",
        },
        "json": {
            "level": "INFO",
            "class": "logging.StreamHandler",
            "formatter": "json",
        },
    },
    "root": {
        "handlers": ["console"],
        "level": "DEBUG",
    },
    "loggers": {
        "django": {
            "handlers": ["json"],
            "level": "INFO",
            "propagate": True,
        },
        "colmena": {
            "handlers": ["file", "json"],
            "level": os.getenv("DJANGO_LOG_LEVEL", "DEBUG"),
            "propagate": False,
        },
        "apps": {
            "handlers": ["file", "json"],
            "level": os.getenv("DJANGO_LOG_LEVEL", "DEBUG"),
            "propagate": False,
        },
    },
    "formatters": {
        "verbose": {
            "format": "{name} level={levelname} {asctime} module={module} process={process:d} thread={thread:d} {message}",
            "style": "{",
        },
        "json": {
            "()": "colmena.logger.formatters.JSONFormatter",
        },
    },
}

# ------------------------------------------------------------------------------
# Security Settings
#
ALLOWED_HOSTS = os.environ.get("ALLOWED_HOSTS", "").split(" ")
CSRF_TRUSTED_ORIGINS = os.environ.get("CSRF_TRUSTED_ORIGINS", "").split(" ")
CORS_ALLOWED_ORIGINS = os.environ.get("CORS_ALLOWED_ORIGINS", "").split(" ")
SECRET_KEY = os.environ.get("COLMENA_SECRET_KEY")

if STAGE == "local":
    CSRF_COOKIE_SECURE = False
    SESSION_COOKIE_SECURE = False

if STAGE in ["dev", "staging"]:
    # SSL required for session/CSRF cookies
    CSRF_COOKIE_SECURE = True
    SESSION_COOKIE_SECURE = True

# ------------------------------------------------------------------------------
# Middleware Settings
#

MIDDLEWARE = MIDDLEWARE + ["whitenoise.middleware.WhiteNoiseMiddleware"]

# ------------------------------------------------------------------------------
# Storage Settings
#

STORAGES = {
    "default": {
        "BACKEND": "django.core.files.storage.FileSystemStorage",
    },
    "staticfiles": {
        "BACKEND": "whitenoise.storage.CompressedManifestStaticFilesStorage",
    },
}

if STAGE == "local":
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.postgresql",
            "NAME": os.environ.get("POSTGRES_DATABASE", "colmena_dev"),
            "USER": os.environ.get("POSTGRES_USERNAME", "postgres"),
            "PASSWORD": os.environ.get("POSTGRES_PASSWORD", "postgres"),
            "HOST": os.environ.get("POSTGRES_HOSTNAME", "localhost"),
            "PORT": os.environ.get("POSTGRES_PORT", "5432"),
        }
    }
if STAGE in ["dev", "staging"]:
    DATABASES = {
        "default": dj_database_url.parse(
            os.environ.get("DATABASE_URL"), conn_max_age=600
        ),
    }

# ------------------------------------------------------------------------------
# Mail Settings
#
if STAGE == "local":
    EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
    EMAIL_HOST = "mail"
    EMAIL_PORT = 1025
    EMAIL_HOST_USER = ""
    EMAIL_HOST_PASSWORD = ""
    EMAIL_USE_SSL = False
    EMAIL_TIMEOUT = 5

elif STAGE in ["dev", "staging"]:
    if os.environ.get("DEBUG_EMAIL", 0):
        EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

    else:
        EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
        EMAIL_HOST = os.environ.get("EMAIL_HOST")
        EMAIL_PORT = os.environ.get("EMAIL_PORT")
        EMAIL_HOST_USER = os.environ.get("EMAIL_HOST_USER")
        EMAIL_HOST_PASSWORD = os.environ.get("EMAIL_HOST_PASSWORD")
        EMAIL_USE_SSL = os.environ.get("EMAIL_USE_SSL") == "1"
        EMAIL_TIMEOUT = 300
