from .base import *

# ------------------------------------------------------------------------------
# Application Settings
#
DEBUG = True
DJANGO_SETTINGS_MODULE = "colmena.settings.test"
USE_COVERAGE = os.environ.get("USE_COVERAGE", "0") == "1"

# ------------------------------------------------------------------------------
# Logger Settings
#

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose-colored",
        },
        "file": {
            "level": "ERROR",
            "class": "logging.FileHandler",
            "filename": "logs/test/error.log",
            "formatter": "verbose",
        },
    },
    "root": {
        "handlers": ["console"],
        "level": "DEBUG",
    },
    "loggers": {
        "django": {
            "handlers": ["console"],
            "level": "INFO",
            "propagate": False,
        },
        "colmena": {
            "handlers": ["console", "file"],
            "level": os.getenv("DJANGO_LOG_LEVEL", "DEBUG"),
            "propagate": False,
        },
        "apps": {
            "handlers": ["console", "file"],
            "level": os.getenv("DJANGO_LOG_LEVEL", "DEBUG"),
            "propagate": False,
        },
    },
    "formatters": {
        "verbose": {
            "format": "{asctime} {levelname} {name} module={module} process={process:d} thread={thread:d} msg='{message}'",
            "style": "{",
        },
        "verbose-colored": {
            "()": "colorlog.ColoredFormatter",
            "format": "%(log_color)s%(asctime)-s %(levelname)-s {%(name)s} [%(module)s] %(message)s",
            "style": "%",
            "log_colors": {
                "DEBUG": "cyan",
                "INFO": "white",
                "WARNING": "yellow",
                "ERROR": "red",
                "CRITICAL": "bold_red",
            },
        },
    },
}

# ------------------------------------------------------------------------------
# Security Settings
#
SECRET_KEY = "colmena-test-secret-key"
CORS_ORIGIN_ALLOW_ALL = True

# ------------------------------------------------------------------------------
# Apps
#
INSTALLED_APPS = INSTALLED_APPS + ["django_extensions", "django_nose"]

# ------------------------------------------------------------------------------
# Database Settings
#
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(os.path.dirname(BASE_DIR), "data/db.sqlite3"),
    }
}

# ------------------------------------------------------------------------------
# Email Settings
#
EMAIL_BACKEND = "django.core.mail.backends.dummy.EmailBackend"

# ------------------------------------------------------------------------------
# Tests
#

# nose has not catched up with python 3.11, so monkeypatching collections.Callable
# module reorganization:
if USE_COVERAGE:
    import collections

    collections.Callable = collections.abc.Callable

    TEST_RUNNER = "django_nose.NoseTestSuiteRunner"

    NOSE_ARGS = [
        "--with-coverage",
        "--cover-package=apps,colmena",
    ]
