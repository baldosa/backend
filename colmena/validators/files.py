import os
from django.core.exceptions import ValidationError
from django.core.files import File
from django.utils.text import format_lazy
from django.utils.translation import gettext_lazy
from colmena.settings.base import IMAGE_MAX_SIZE

AVAILABLE_TYPES = [
    {"ext": "avi", "content_type": "video/x-msvideo"},
    {"ext": "bmp", "content_type": "image/bmp"},
    {"ext": "csv", "content_type": "text/csv"},
    {"ext": "doc", "content_type": "application/msword"},
    {
        "ext": "docx",
        "content_type": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    },
    {"ext": "dwg", "content_type": "image/vnd.dwg"},
    {"ext": "flv", "content_type": "video/x-flv"},
    {"ext": "gif", "content_type": "image/gif"},
    {"ext": "jpg", "content_type": "image/jpeg"},
    {"ext": "jpeg", "content_type": "image/jpeg"},
    {"ext": "mdb", "content_type": "application/x-msaccess"},
    {"ext": "mid", "content_type": "audio/midi"},
    {"ext": "mov", "content_type": "video/quicktime"},
    {"ext": "mp3", "content_type": "audio/mpeg"},
    {"ext": "mp4", "content_type": "video/mp4"},
    {"ext": "mpeg", "content_type": "video/mpeg"},
    {"ext": "odp", "content_type": "application/vnd.oasis.opendocument.presentation"},
    {"ext": "ods", "content_type": "application/vnd.oasis.opendocument.spreadsheet"},
    {"ext": "odt", "content_type": "application/vnd.oasis.opendocument.text"},
    {"ext": "png", "content_type": "image/png"},
    {"ext": "pdf", "content_type": "application/pdf"},
    {"ext": "ppt", "content_type": "application/vnd.ms-powerpoint"},
    {"ext": "rar", "content_type": "application/x-rar-compressed"},
    {"ext": "svg", "content_type": "image/svg+xml"},
    {"ext": "tiff", "content_type": "image/tiff"},
    {"ext": "wav", "content_type": "audio/wav"},
    {"ext": "wma", "content_type": "audio/x-ms-wma"},
    {"ext": "xsl", "content_type": "application/xml"},
    {"ext": "zip", "content_type": "application/x-zip-compressed"},
    {"ext": "weba", "content_type": "audio/webm"},
]

AVAILABLE_PROJECT_TYPES = [
    {"ext": "zip", "content_type": "application/x-zip-compressed"},
]


def get_available_extensions():
    return AVAILABLE_TYPES


def get_extension_in_list(list: list):
    available_extensions = [type["ext"] for type in list]
    return available_extensions


def is_valid_extension(extension):
    return extension in get_extension_in_list(AVAILABLE_TYPES)


def is_project_extension(extension):
    return extension in get_extension_in_list(AVAILABLE_PROJECT_TYPES)


def get_filename_extension(filename):
    name, extension = os.path.splitext(filename)
    extension = extension.lower().replace(".", "")
    return name, extension


def validate_max_size(value: File):
    filesize = value.size
    max_size = IMAGE_MAX_SIZE * 1024 * 1024
    if filesize > max_size:
        raise ValidationError(
            format_lazy(
                "{text} {size} MB",
                text=gettext_lazy("Max file size is"),
                size=IMAGE_MAX_SIZE,
            )
        )
