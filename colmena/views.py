from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import AllowAny

from drf_spectacular.utils import (
    extend_schema,
    OpenApiExample,
    OpenApiResponse,
    OpenApiTypes,
)

from colmena.settings.base import SPECTACULAR_SETTINGS


@extend_schema(
    request=None,
    responses={
        200: OpenApiResponse(
            response=OpenApiTypes.OBJECT,
            description="A status message indicating the service is OK.",
            examples=[
                OpenApiExample(
                    name="Example 1",
                    value={"status_code": 200, "value": {"status": "ok"}},
                )
            ],
        )
    },
    description="Check the status of the service.",
)
class StatusView(APIView):
    permission_classes = [AllowAny]

    def get(self, request, *args, **kwargs):
        return Response({"status": "ok"}, status=status.HTTP_200_OK)


@extend_schema(
    request=None,
    responses={
        200: OpenApiResponse(
            response=OpenApiTypes.OBJECT,
            description="A message indicating the OpenApi schema version.",
            examples=[
                OpenApiExample(
                    name="Example 1",
                    value={
                        "status_code": 200,
                        "value": {"version": "1.0.0"},
                    },
                )
            ],
        )
    },
    description="Check the OpenApi schema version.",
)
class OpenApiSchemaView(APIView):
    permission_classes = [AllowAny]

    def get(self, request, *args, **kwargs):
        version = SPECTACULAR_SETTINGS.get("VERSION")
        return Response({"version: ": version}, status=status.HTTP_200_OK)
